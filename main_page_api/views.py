import aiohttp_jinja2
from aiohttp import web


class MainPage(web.View):

    async def get(self):
        response = aiohttp_jinja2.render_template('base.html', self.request, context={})
        response.headers['Content-Language'] = 'en'
        return response
