from main_page_api.views import MainPage

routes = [
    ('GET', '/', MainPage, 'index'),
    ('GET', '/id{user_id}/', MainPage, 'user_profile'),
    ('GET', '/id{user_id}/stream/', MainPage, 'stream'),
    ('GET', '/id{user_id}/messages/', MainPage, 'messages'),
    ('GET', '/id{user_id}/about/', MainPage, 'about'),
]
