/* global document:true */
/* eslint no-undef: "error" */

import 'babel-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import createHistory from 'history/createBrowserHistory';
import { ConnectedRouter } from 'react-router-redux';

// import { mainRoutes } from './routes';
import configureStore from './store/configureStore';
import App from './containers/App';


const history = createHistory();
const store = configureStore(history);

ReactDOM.render(
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <div>
                <App />
            </div>
        </ConnectedRouter>
    </Provider>,
    document.getElementById('page-wrapper'),
);