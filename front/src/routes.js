import React from 'react';
import { Route, Switch } from 'react-router-dom';

import App from './containers/App';

import RegisterForm from './containers/RegisterForm';
import UserProfilePage from './containers/UserProfilePage';
import MainPage from './components/MainPage';

export const mainRoutes = (
    <Switch>
        <Route exact path='/' component={App} />
        <Route path='/main' component={MainPage} />
        <Route path='/registration/' component={RegisterForm} />
        <Route path='/id:userId/' component={UserProfilePage} />
    </Switch>
);
