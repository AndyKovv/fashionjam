import { createStore, applyMiddleware, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { routerMiddleware } from 'react-router-redux';
import { rootReducer } from '../reducers';

export default function configureStore(history) {
    const reduxRouterMiddleware = routerMiddleware(history);
    const store = compose(
        applyMiddleware(thunkMiddleware),
        applyMiddleware(reduxRouterMiddleware),
    )(createStore)(rootReducer);
    return store;
}