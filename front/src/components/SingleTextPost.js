import React from 'react';

export default class SinglePost extends React.Component {
    render() {
        return (
            <div className='item-post-wrap single-post'>
                <div className='item-post'>
                    <div className='item-post-header'>
                        <div className='user-post'>
                            <a href='#' className='user-img'>
                                <img src='/static/images/img1.jpg' alt='' />
                            </a>
                            <div className='text-wrp'>
                                <div className='name'>Denise King</div>
                                <div className='data'>30.06.2016<svg><use xlinkHref='#icon-calendar' /></svg>14:10</div>
                            </div>
                        </div>
                    </div>
                    <div className='item-post-content'>
                        <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classNameical Latin literature from 45 BC, making it over 2000 years old</p>
                    </div>
                    <div className='item-post-foot'>
                        <ul className='activity'>
                            <li><a className='share' href='#'><svg><use xlinkHref='#icon-share' /></svg></a></li>
                            <li><a className='comment' href='#'><svg><use xlinkHref='#icon-comment' /></svg><span>12</span></a></li>
                            <li><a className='like' href='#'><span>124</span> <span className='like-icon'><svg><use xlinkHref='#icon-like' /></svg></span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}