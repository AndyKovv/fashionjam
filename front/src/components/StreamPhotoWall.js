import React from 'react';

export default class StreamPhotoWall extends React.Component {
    render() {
        return (
            <div className='colection-top mod-full'>
                <div className='wrap-md'>
                    <div className='colection-item mod-sm' style={{ backgroundImage: 'url( "/static/images/portfolio/1.jpg" )' }} />
                    <div className='colection-item mod-sm' style={{ backgroundImage: 'url( "/static/images/portfolio/2.jpg" )' }} />
                    <div className='colection-item mod-sm' style={{ backgroundImage: 'url( "/static/images/portfolio/3.jpg" )' }} />
                    <div className='colection-item mod-sm' style={{ backgroundImage: 'url( "/static/images/portfolio/4.jpg" )' }} />
                </div>
                <div className='wrap-md'>
                    <div className='colection-item' style={{ backgroundImage: 'url( "/static/images/portfolio/8.jpg" )' }} />
                </div>
                <div className='wrap-lg'>
                    <div className='colection-item mod-sm' style={{ backgroundImage: 'url( "/static/images/portfolio/5.jpg" )' }} />
                    <div className='colection-item mod-sm' style={{ backgroundImage: 'url( "/static/images/portfolio/6.jpg" )' }} />
                    <div className='colection-item mod-sm' style={{ backgroundImage: 'url( "/static/images/portfolio/7.jpg" )' }} />
                    <div className='colection-item mod-sm' style={{ backgroundImage: 'url( "/static/images/portfolio/11.jpg" )' }} />
                    <div className='colection-item mod-sm' style={{ backgroundImage: 'url( "/static/images/portfolio/9.jpg" )' }} />
                    <div className='colection-item mod-sm' style={{ backgroundImage: 'url( "/static/images/portfolio/10.jpg" )' }} />
                </div>
            </div>
        );
    }
}