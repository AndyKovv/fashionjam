import React from 'react';

export default class StreamSidebar extends React.Component {
    render() {
        return (
            <div className='sidebar'>
                <div className='section-sidebar'>
                    <h2 className='title-sidebar'>Popular Users</h2>
                    <ul className='user-list'>
                        <li><a className='user-pick' href='#'><img src='/static/images/img-12.jpg' alt='' /></a></li>
                        <li><a className='user-pick' href='#'><img src='/static/images/img-12.jpg' alt='' /></a></li>
                        <li><a className='user-pick' href='#'><img src='/static/images/img-12.jpg' alt='' /></a></li>
                        <li><a className='user-pick' href='#'><img src='/static/images/img-12.jpg' alt='' /></a></li>
                        <li><a className='user-pick' href='#'><img src='/static/images/img-12.jpg' alt='' /></a></li>
                        <li><a className='user-pick' href='#'><img src='/static/images/img-12.jpg' alt='' /></a></li>
                    </ul>
                </div>
                <div className='section-sidebar'>
                    <h2 className='title-sidebar'>Videos </h2>
                    <div className='portfolio'>
                        <a href='ajax-video.php' data-fancybox-type='ajax' data-id='id' className='item-video'>
                            <span className='img-wrap'>
                                <img src='/static/images/video/1.png' alt='img' />
                            </span>
                            <span className='name'>My magic in my paint</span>
                        </a>
                    </div>
                </div>
                <div className='section-sidebar'>
                    <h2 className='title-sidebar'>Popular projeCts</h2>
                    <ul className='list-projects'>
                        <li><a href='#' className='pick'><img src='/static/images/img-12.jpg' alt='' /></a> <span>MUA, of the creative type needed, In Utrecht Now!</span></li>
                        <li><a href='#' className='pick'><img src='/static/images/img-12.jpg' alt='' /></a> <span>PAID Lingerie/Intimates Shoot for Fashion Book</span></li>
                        <li><a href='#' className='pick'><img src='/static/images/img-12.jpg' alt='' /></a> <span>Sexy Model Needed For Las Vegas Bar and Nightclub Trade Show.</span></li>
                    </ul>
                </div>
            </div>
        );
    }
}