import React from 'react';

export default class PostFilter extends React.Component {
    render() {
        return (
            <div className='wrap-filter'>
                <ul className='first-filt'>
                    <li className='active'><a href='#'>Global News</a></li>
                    <li><a href='#'>Followed</a></li>
                </ul>
                <ul className='two-filt'>
                    <li className='active'><a href='#'>Show All</a></li>
                    <li><a href='#'>Only Photos</a></li>
                    <li><a href='#'>Only Videos</a></li>
                    <li><a href='#'>ONLY PROJECTS</a></li>
                </ul>
            </div>
        );
    }
}