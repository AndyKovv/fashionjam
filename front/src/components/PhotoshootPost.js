import React from 'react';

export default class PhotoshootPost extends React.Component {
    render() {
        return (
            <div className='item-post-wrap photoshoot'>
                <div className='item-post'>
                    <div className='item-post-header'>
                        <div className='user-post'>
                            <a href='#' className='user-img'>
                                <img src='/static/images/img1.jpg' alt='' />
                            </a>
                            <div className='text-wrp'>
                                <div className='shared-head'>
                                    <div className='name'>Denise King</div>
                                    <div className='share-to'><a className='share' href='/login/'><svg><use xlinkHref='#icon-share' /></svg></a></div>
                                    <div className='name'>Denise Gold</div>
                                </div>
                                <div className='data'>30.06.2016<svg><use xlinkHref='#icon-calendar' /></svg>14:10</div>
                            </div>
                        </div>
                    </div>
                    <div className='item-post-content'>
                        <a href='#' className='title-post'>Looking for models to Snapshot style Fashion and Editorial</a>
                        <p>The majority have suffered alteration in some form, by injected humour</p>
                        <a className='img-post' href='#'>
                            <img src='/static/images/img11.jpg' alt='' />
                        </a>
                        <div className='dops-inf'>
                            <ul className='dops-inf-container'>
                                <li className='location'><svg><use xlinkHref='#icon-location' /></svg>Los Angeles, USA</li>
                                <li className='price'><svg><use xlinkHref='#icon-price' /></svg>$8000</li>
                                <li className='time'><svg><use xlinkHref='#icon-time' /></svg>30.06.16</li>
                            </ul>
                        </div>
                    </div>
                    <div className='item-post-foot'>
                        <ul className='activity'>
                            <li><a className='share' href='#'><svg><use xlinkHref='#icon-share' /></svg></a></li>
                            <li><a className='comment' href='#'><svg><use xlinkHref='#icon-comment' /></svg><span>12</span></a></li>
                            <li><a className='like' href='#'><span>124</span> <span className='like-icon'><svg><use xlinkHref='#icon-like' /></svg></span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}