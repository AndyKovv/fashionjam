import React from 'react';

export default class AppHeader extends React.Component {
    renderHeader() {
        /* Method should render header if user is authenticated otherwise empty */
        const userIsAuthenticated = this.props.user && this.props.user.userIsAuthenticated;
        if (!userIsAuthenticated) {
            return <div />;
        }
        return (
            <div>
                <div className='col-xs-2 col-md-2 des-hide'>
                    <a className='more' href='#'><svg><use xlinkHref='#icon-more' /></svg></a>
                </div>
                <div className='col-xs-6 col-md-8 col-lg-8 mob-hidden js-mob-nav'>
                    <form className='search' action='#'>
                        <input type='text' placeholder='SEARCH' />
                        <button className='btn'><svg><use xlinkHref='#icon-search' /></svg></button>
                    </form>
                    <nav className='navigation'>
                        <ul>
                            <li className='active'><a href='home.html'>HOME</a></li>
                            <li><a href='browse.html'>BROWSE</a></li>
                            <li><a href='casting-calls.html'>CASTING CALLS</a></li>
                            <li><a href='stream.html'>stream</a></li>
                        </ul>
                    </nav>
                </div>
                <div className='col-xs-10 col-md-3 col-lg-4'>
                    <a className='burger' href='#'><svg><use xlinkHref='#icon-burger' /></svg></a>
                    <ul className='activity'>
                        <li>
                            <a href='messages.html' className='message'>
                                <svg><use xlinkHref='#icon-message' /></svg>
                                <span>3</span>
                            </a>
                        </li>
                        <li>
                            <a href='notifications.html' className='notification'>
                                <svg><use xlinkHref='#icon-notification' /></svg>
                                <span>5</span>
                            </a>
                        </li>
                    </ul>
                    <a href='index.html' className='user'>
                        <span className='wrap-img'>
                            <img src='images/img1.jpg' alt='' />
                        </span>
                        <span className='name'>Alexis</span>
                    </a>
                </div>
            </div>
        );
    }

    render() {
        return (
            <div>
                <header id='header' className='wrapper header'>
                    <div className='container'>
                        <div className='row'>
                            {this.renderHeader()}
                        </div>
                    </div>
                </header>
                <div className='overflow' />
            </div>
        );
    }
}