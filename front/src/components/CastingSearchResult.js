import React from 'react';

export default class CastingSearchResult extends React.Component {
    render() {
        return (
            <div className='mod-content mod-browse'>
                <div className='row'>
                    <h2 className='title-page'>SEARCH RESULTS</h2>
                </div>
                <div className='casting-item row'>
                    <div className='col-sm-8'>
                        <div className='media-left'>
                            <div className='user-pick'>
                                <img src='/static/images/img1.jpg' alt='' />
                            </div>
                            <div className='user'>
                                <div className='user-name'>Emily Larson</div>
                                <div className='user-prof'>MUA, of the creative type needed, In Utrecht Now!</div>
                                <ul className='user-info'>
                                    <li><svg><use xlinkHref='#icon-sye' /></svg>Looking for: <span>Make up Artists.</span></li>
                                    <li><svg><use xlinkHref='#icon-location' /></svg>Location: <span>Turkey, Istanbul.</span></li>
                                    <li><svg><use xlinkHref='#icon-cub' /></svg>Rate: <span>$100.</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className='col-sm-4'>
                        <div className='media-right'>
                            <div className='data'><svg><use xlinkHref='#icon-calendar' /></svg>13.08.2016 - 28.08.2016</div>
                            <div className='btn-wrap'>
                                <a href='#test-modal' className='el-btn bg-none js-modal-casting'>READ MORE</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='casting-item row'>
                    <div className='col-sm-8'>
                        <div className='media-left'>
                            <div className='user-pick'>
                                <img src='/static/images/img1.jpg' alt='' />
                            </div>
                            <div className='user'>
                                <div className='user-name'>Emily Larson</div>
                                <div className='user-prof'>MUA, of the creative type needed, In Utrecht Now!</div>
                                <ul className='user-info'>
                                    <li><svg><use xlinkHref='#icon-sye' /></svg>Looking for: <span>Make up Artists.</span></li>
                                    <li><svg><use xlinkHref='#icon-location' /></svg>Location: <span>Turkey, Istanbul.</span></li>
                                    <li><svg><use xlinkHref='#icon-cub' /></svg>Rate: <span>$100.</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className='col-sm-4'>
                        <div className='media-right'>
                            <div className='data'><svg><use xlinkHref='#icon-calendar' /></svg>13.08.2016 - 28.08.2016</div>
                            <div className='btn-wrap'>
                                <a href='#test-modal' className='el-btn bg-none js-modal-casting'>READ MORE</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='casting-item row'>
                    <div className='col-sm-8'>
                        <div className='media-left'>
                            <div className='user-pick'>
                                <img src='/static/images/img1.jpg' alt='' />
                            </div>
                            <div className='user'>
                                <div className='user-name'>Emily Larson</div>
                                <div className='user-prof'>MUA, of the creative type needed, In Utrecht Now!</div>
                                <ul className='user-info'>
                                    <li><svg><use xlinkHref='#icon-sye' /></svg>Looking for: <span>Make up Artists.</span></li>
                                    <li><svg><use xlinkHref='#icon-location' /></svg>Location: <span>Turkey, Istanbul.</span></li>
                                    <li><svg><use xlinkHref='#icon-cub' /></svg>Rate: <span>$100.</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className='col-sm-4'>
                        <div className='media-right'>
                            <div className='data'><svg><use xlinkHref='#icon-calendar' /></svg>13.08.2016 - 28.08.2016</div>
                            <div className='btn-wrap'>
                                <a href='#test-modal' className='el-btn bg-none js-modal-casting'>READ MORE</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='casting-item row'>
                    <div className='col-sm-8'>
                        <div className='media-left'>
                            <div className='user-pick'>
                                <img src='/static/images/img1.jpg' alt='' />
                            </div>
                            <div className='user'>
                                <div className='user-name'>Emily Larson</div>
                                <div className='user-prof'>MUA, of the creative type needed, In Utrecht Now!</div>
                                <ul className='user-info'>
                                    <li><svg><use xlinkHref='#icon-sye' /></svg>Looking for: <span>Make up Artists.</span></li>
                                    <li><svg><use xlinkHref='#icon-location' /></svg>Location: <span>Turkey, Istanbul.</span></li>
                                    <li><svg><use xlinkHref='#icon-cub' /></svg>Rate: <span>$100.</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className='col-sm-4'>
                        <div className='media-right'>
                            <div className='data'><svg><use xlinkHref='#icon-calendar' /></svg>13.08.2016 - 28.08.2016</div>
                            <div className='btn-wrap'>
                                <a href='#test-modal' className='el-btn bg-none js-modal-casting'>READ MORE</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='casting-item row'>
                    <div className='col-sm-8'>
                        <div className='media-left'>
                            <div className='user-pick'>
                                <img src='/static/images/img1.jpg' alt='' />
                            </div>
                            <div className='user'>
                                <div className='user-name'>Emily Larson</div>
                                <div className='user-prof'>MUA, of the creative type needed, In Utrecht Now!</div>
                                <ul className='user-info'>
                                    <li><svg><use xlinkHref='#icon-sye' /></svg>Looking for: <span>Make up Artists.</span></li>
                                    <li><svg><use xlinkHref='#icon-location' /></svg>Location: <span>Turkey, Istanbul.</span></li>
                                    <li><svg><use xlinkHref='#icon-cub' /></svg>Rate: <span>$100.</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className='col-sm-4'>
                        <div className='media-right'>
                            <div className='data'><svg><use xlinkHref='#icon-calendar' /></svg>13.08.2016 - 28.08.2016</div>
                            <div className='btn-wrap'>
                                <a href='#test-modal' className='el-btn bg-none js-modal-casting'>READ MORE</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='casting-item row'>
                    <div className='col-sm-8'>
                        <div className='media-left'>
                            <div className='user-pick'>
                                <img src='/static/images/img1.jpg' alt='' />
                            </div>
                            <div className='user'>
                                <div className='user-name'>Emily Larson</div>
                                <div className='user-prof'>MUA, of the creative type needed, In Utrecht Now!</div>
                                <ul className='user-info'>
                                    <li><svg><use xlinkHref='#icon-sye' /></svg>Looking for: <span>Make up Artists.</span></li>
                                    <li><svg><use xlinkHref='#icon-location' /></svg>Location: <span>Turkey, Istanbul.</span></li>
                                    <li><svg><use xlinkHref='#icon-cub' /></svg>Rate: <span>$100.</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className='col-sm-4'>
                        <div className='media-right'>
                            <div className='data'><svg><use xlinkHref='#icon-calendar' /></svg>13.08.2016 - 28.08.2016</div>
                            <div className='btn-wrap'>
                                <a href='#test-modal' className='el-btn bg-none js-modal-casting'>READ MORE</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='casting-item row'>
                    <div className='col-sm-8'>
                        <div className='media-left'>
                            <div className='user-pick'>
                                <img src='/static/images/img1.jpg' alt='' />
                            </div>
                            <div className='user'>
                                <div className='user-name'>Emily Larson</div>
                                <div className='user-prof'>MUA, of the creative type needed, In Utrecht Now!</div>
                                <ul className='user-info'>
                                    <li><svg><use xlinkHref='#icon-sye' /></svg>Looking for: <span>Make up Artists.</span></li>
                                    <li><svg><use xlinkHref='#icon-location' /></svg>Location: <span>Turkey, Istanbul.</span></li>
                                    <li><svg><use xlinkHref='#icon-cub' /></svg>Rate: <span>$100.</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className='col-sm-4'>
                        <div className='media-right'>
                            <div className='data'><svg><use xlinkHref='#icon-calendar' /></svg>13.08.2016 - 28.08.2016</div>
                            <div className='btn-wrap'>
                                <a href='#test-modal' className='el-btn bg-none js-modal-casting'>READ MORE</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}