import React from 'react';
import PhotoshootPost from '../components/PhotoshootPost';
import SinglePost from '../components/SingleTextPost';
import VideoPost from '../components/VideoPost';
import EventPost from '../components/EventPost';
import ImgPost from '../components/ImgPost';

export default class PostWall extends React.Component {
    render() {
        return (
            <div className='wrap-posts js-posts mod-two'>
                <div className='grid-sizer' />
                <PhotoshootPost />
                <SinglePost />
                <VideoPost />
                <EventPost />
                <SinglePost />
                <ImgPost />
            </div>
        );
    }
}