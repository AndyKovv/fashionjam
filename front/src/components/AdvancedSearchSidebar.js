import React from 'react';

export default class AdvancedSearchSidebar extends React.Component {
    render() {
        return (
            <div className=''>
                <h2 className='title-page'>Advanced Search</h2>
                <div className='col-md-12'>
                    <label htmlFor='type' className='el-label'>
                        Type
                        <select className='ui dropdown' id='type'>
                            <option value=''>Select</option>
                            <option value='1'>Select 1</option>
                            <option value='0'>Select 2</option>
                        </select>
                    </label>
                </div>
                <div className='col-md-12'>
                    <label htmlFor='country' className='el-label'>
                        Country
                        <select className='ui dropdown' id='country'>
                            <option value=''>Select</option>
                            <option value='1'>Select 1</option>
                            <option value='0'>Select 2</option>
                        </select>
                    </label>
                </div>
                <div className='col-md-6'>
                    <label htmlFor='age' className='el-label'>
                        Age
                        <select className='ui dropdown' id='age'>
                            <option value=''>From</option>
                            <option value='1'>Select 1</option>
                            <option value='0'>Select 2</option>
                        </select>
                    </label>
                </div>
                <div className='col-md-6 mod-mob'>
                    <label htmlFor='ageTo' className='el-label'>
                        &nbsp;
                        <select className='ui dropdown' id='ageTo'>
                            <option value=''>To</option>
                            <option value='1'>Select 1</option>
                            <option value='0'>Select 2</option>
                        </select>
                    </label>
                </div>
                <div className='col-md-12'>
                    <div className='el-label'>
                        Gender
                        <div className='radio-list'>
                            <label htmlFor='male' className='el-radio'>
                                <input type='radio' name='size' checked id='male' />
                                <span className='radio-icon' />
                                Male
                            </label>
                            <label htmlFor='female' className='el-radio'>
                                <input type='radio' name='size' id='female' />
                                <span className='radio-icon' />
                                Female
                            </label>
                            <label htmlFor='any' className='el-radio'>
                                <input type='radio' name='size' id='any' />
                                <span className='radio-icon' />
                                Any
                            </label>
                        </div>
                    </div>
                </div>
                <div className='col-md-12 text-center start-search'>
                    <a href='#' className='el-btn'>START SEARCHING</a>
                    <h5>365 25</h5>
                    <span>active members</span>
                </div>
            </div>
        );
    }
}