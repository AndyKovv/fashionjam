/* eslint class-methods-use-this: 0 */

import React from 'react';
import OwlCarousel from 'react-owl-carousel';
import { Link, Route, withRouter, Switch } from 'react-router-dom';

import RegisterForm from '../containers/RegisterForm';
import LoginForm from '../containers/LoginForm';


class MainPage extends React.Component {
    constructor(props) {
        super(props);
        this.openRegisterModal = this.openRegisterModal.bind(this);
    }
    componentDidMount() {
    }
    getSliderOptions() {
        return {
            loop: true,
            animateOut: 'fadeOut',
            mouseDrag: false,
            autoplay: true,
            autoplayTimeout: 1000,
            autoplayHoverPause: true,
            margin: 0,
            nav: true,
            items: 1,
            navText: ['',''], // eslint-disable-line
        };
    }
    openRegisterModal(e) {
        /* Method should open register modal */
        e.preventDefault();
        this.props.registerActions.modalOpen();
        return true;
    }
    render() {
        const canOpenModal = this.props.register && this.props.register.openModal;
        return (
            <div className='wrap-home-slider'>
                <OwlCarousel className='home-slider owl-carousel' {...this.getSliderOptions()}>
                    <div className='item' style={{ backgroundImage: 'url( "/static/images/home.jpg" )' }} />
                    <div className='item' style={{ backgroundImage: 'url( "/static/images/home1.jpg" )' }} />
                </OwlCarousel>
                {canOpenModal && <RegisterForm {...this.props} />}
                <Switch>
                    <Route exact path='/login/' render={() => <LoginForm {...this.props} />} />
                </Switch>
                <div className='container'>
                    <div className='row'>
                        <div className='col-md-12'>
                            <h1>fashion<span>jam</span></h1>
                            <ul className='btn-wrap-home'>
                                <li><Link className='el-btn' to='/login/'>LOG IN</Link></li>
                                <li><Link className='el-btn js-modal-link' to='#' onClick={this.openRegisterModal}>SIGN UP</Link></li>
                                <li><a className='el-btn mod-fb' href='/facebook/'><img src='/static/images/fb.png' alt='' /> Continue with FACEBOOK </a></li>
                            </ul>
                            <h3>welcome to the world fashion network</h3>
                            <h4>Photo by Antony Manson</h4>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default withRouter(MainPage);
