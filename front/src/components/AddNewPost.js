import React from 'react';

export default class AddPost extends React.Component {
    render() {
        return (
            <div className='add-new-post'>
                <form action='#'>
                    <input type='text' name='post' placeholder='WHATS NEW' />
                    <a href='#' className='plus'><svg><use xlinkHref='#icon-plus' /></svg></a>
                    <button className='el-btn' type='submit'>POST</button>
                </form>
            </div>
        );
    }
}