import React from 'react';
import UserInfoDesc from '../components/UserInfoDesc';

export default class UserInfoForm extends React.Component {
    render() {
        return (
            <div className='content'>
                <div className='about-content mod-content'>
                    <div className='row'>
                        <h2 className='title-page '>ABOUT ME</h2>
                    </div>
                    <div className='row'>
                        <div className='col-md-3'>
                            <label htmlFor='name' className='el-label'>
                                First Name
                                <input type='text' id='name' value='Denice' />
                            </label>
                        </div>
                        <div className='col-md-3'>
                            <label htmlFor='king' className='el-label'>
                                King
                                <input type='text' id='king' value='Denice' />
                            </label>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='col-md-3'>
                            <label htmlFor='memberType' className='el-label'>
                                Member Type
                                <input type='text' id='memberType' value='Photographer' />
                            </label>
                        </div>
                        <div className='col-md-3'>
                            <label htmlFor='gender' className='el-label'>
                                Gender
                                <input type='text' name='gender' value='Female' />
                            </label>
                        </div>
                        <div className='col-md-2'>
                            <label htmlFor='age' className='el-label'>
                                Age
                                <input type='text' id='age' value='30' />
                            </label>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='col-md-3'>
                            <label htmlFor='city' className='el-label'>
                                City
                                <select id='city' className='ui dropdown'>
                                    <option value=''>Los Angeles</option>
                                    <option value='1'>City 1</option>
                                    <option value='0'>City 2</option>
                                </select>
                            </label>
                        </div>
                        <div className='col-md-3'>
                            <label htmlFor='state' className='el-label'>
                                State
                                <select id='state' className='ui dropdown'>
                                    <option value=''>Illinois</option>
                                    <option value='1'>State 1</option>
                                    <option value='0'>State 2</option>
                                </select>
                            </label>
                        </div>
                        <div className='col-md-3'>
                            <label htmlFor='country' className='el-label'>
                                Country
                                <select id='country' className='ui dropdown'>
                                    <option value=''>USA</option>
                                    <option value='1'>Country 1</option>
                                    <option value='0'>Country 2</option>
                                </select>
                            </label>
                        </div>
                        <div className='col-md-2'>
                            <label htmlFor='zip' className='el-label'>
                                ZIP Code
                                <input type='text' id='zip' value='29000' />
                            </label>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='col-md-3'>
                            <label htmlFor='joined' className='el-label calendar'>
                                Joined
                                <input type='text' id='joined' value='25.03.2014' />
                            </label>
                        </div>
                        <div className='col-md-3'>
                            <label htmlFor='experience' className='el-label'>
                                Experience
                                <input type='text' id='experience' value='Experienced' />
                            </label>
                        </div>
                        <div className='col-md-3'>
                            <label htmlFor='compensation' className='el-label'>
                                Compensation
                                <input type='text' id='compensation' value='Any' />
                            </label>
                        </div>
                    </div>
                    <UserInfoDesc />
                </div>
            </div>
        );
    }
}