import React from 'react';

export default class UserBgGllery extends React.Component {
    render() {
        return (
            <div className='colection-top'>
                <div className='wrap-sm'>
                    <div className='colection-item'>
                        <img src='/static/images/img2.jpg' alt='' />
                    </div>
                    <div className='colection-item'>
                        <img src='/static/images/img3.jpg' alt='' />
                    </div>
                </div>
                <div className='wrap-md'>
                    <div className='colection-item'>
                        <img src='/static/images/img1.jpg' alt='' />
                    </div>
                </div>
                <div className='wrap-md'>
                    <div className='colection-item mod-sm'>
                        <img src='/static/images/img4.jpg' alt='' />
                    </div>
                    <div className='colection-item mod-sm'>
                        <img src='/static/images/img5.jpg' alt='' />
                    </div>
                    <div className='colection-item mod-sm'>
                        <img src='/static/images/img6.jpg' alt='' />
                    </div>
                    <div className='colection-item mod-sm'>
                        <img src='/static/images/img7.jpg' alt='' />
                    </div>
                </div>
            </div>
        );
    }
}