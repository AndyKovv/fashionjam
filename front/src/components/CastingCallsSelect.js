import React from 'react';

export default class CastingCallsSelect extends React.Component {
    render() {
        return (
            <div className='mod-content'>
                <div className='row'>
                    <div className='col-sm-4 item-cast'>
                        <h3>Casting call</h3>
                        <p>Find  members <br />for a specific project</p>
                        <a href='#' className='el-btn'>SELECT</a>
                    </div>
                    <div className='col-sm-4 item-cast'>
                        <h3>Travel Notice</h3>
                        <p>Connect with other members <br /> while traveling</p>
                        <a href='#' className='el-btn'>SELECT</a>
                    </div>
                    <div className='col-sm-4 item-cast'>
                        <h3>Availability Notice</h3>
                        <p>Post your availability <br />to be considered for local projects</p>
                        <a href='#' className='el-btn'>SELECT</a>
                    </div>
                </div>
            </div>
        );
    }
}