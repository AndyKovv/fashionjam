import React from 'react';

export default class SearchInput extends React.Component {
    render() {
        return (
            <div className='mod-content none-overflov'>
                <div className='row'>
                    <div className='col-sm-6 col-md-7'>
                        <form action='#' className='search'>
                            <input type='text' placeholder='Search' />
                        </form>
                    </div>
                    <div className='col-md-5 sort-wrap'>
                        <span>Sort By</span>
                        <select className='ui dropdown'>
                            <option value=''>Select</option>
                            <option value='1'>Select 1</option>
                            <option value='0'>Select 2</option>
                        </select>
                        <ul className='sort'>
                            <li><a href='#'><svg><use xlinkHref='#icon-sort-1' /></svg></a></li>
                            <li><a href='#'><svg><use xlinkHref='#icon-sort-2' /></svg></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}