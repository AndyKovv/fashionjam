import React from 'react';

export default class UserInfoDesc extends React.Component {
    render() {
        return (
            <div className='row'>
                <div className='col-md-12'>
                    <p className='el-label'>Description</p>
                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don`t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isnt anything embarrassing hidden in the middle of text</p>
                </div>
            </div>
        );
    }
}