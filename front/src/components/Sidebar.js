import React from 'react';
import SidebarMenu from '../components/SidebarMenu';

export default class Sidebar extends React.Component {
    render() {
        return (
            <div className='sidebar'>
                <div className='user-img' style={{ backgroundImage: 'url( "/static/images/img1.jpg" )' }} />
                <div className='user-name'>
                    Alexis Connor Photographer
                </div>
                <div className='text-center'>
                    <ul className='social'>
                        <li><a href='#'><svg><use xlinkHref='#icon-soc-1' /></svg></a></li>
                        <li><a href='#'><svg><use xlinkHref='#icon-soc-2' /></svg></a></li>
                        <li><a href='#'><svg><use xlinkHref='#icon-soc-3' /></svg></a></li>
                        <li><a href='#'><svg><use xlinkHref='#icon-soc-4' /></svg></a></li>
                        <li><a href='#'><svg><use xlinkHref='#icon-soc-5' /></svg></a></li>
                    </ul>
                </div>
                <div className='location'><svg><use xlinkHref='#icon-location' /></svg> Los Angeles, USA</div>
                <a href='dribbble.com/DeniseKing' className='site' target='_blank'><svg><use xlinkHref='#icon-site' /></svg>dribbble.com/DeniseKing</a>
                <div className='folow'>
                    <div className='folow-item'>
                        <div className='number'>1,286</div>
                        <div className='name'>followers</div>
                        <a href='#' className='el-btn'>FOLLOW</a>
                    </div>
                    <div className='folow-item'>
                        <div className='number'>86</div>
                        <div className='name'>following</div>
                        <a href='#send-message' className='el-btn js-modal-link'>MESSAGE</a>
                    </div>
                </div>
                <SidebarMenu />
            </div>
        );
    }
}
