import React from 'react';

export default class MessagesWall extends React.Component {
    render() {
       return (
            <div className='content'>
                <div className='mod-content'>
                    <div className='row'>
                        <h2 className='title-page'>MESSAGES</h2>
                    </div>
                    <div className='row'>
                        <ul className='col-md-12 tabs-head'>
                            <li>
                                <label htmlFor="inputName" className='el-checkbox none-text'>
                                    <input type='checkbox' name='size' checked />
                                    <span className='checkbox-icon' />
                                </label>
                            </li>
                            <li className='active'><a href='#'>Inbox  <span>86</span></a></li>
                            <li><a href='#'>outbox <span>12</span></a></li>
                            <li><a href='#'>Trash <span>5</span></a></li>
                            <li><a href='#'>Spam <span>1</span></a></li>
                        </ul>
                    </div>
                    <div className='messages-content'>
                        <div className='item-massage'>
                            <div className='check'>
                                <label htmlFor="inputName" className='el-checkbox none-text'>
                                    <input type='checkbox' name='size' />
                                    <span className='checkbox-icon' />
                                </label>
                            </div>
                            <div className='img'>
                                <div className='user-pick'>
                                    <img src='/static/images/img-12.jpg' alt='' />
                                </div>
                            </div>
                            <div className='name'>
                                <a href='#' className='user-name'><span>Emily Larson</span></a>
                            </div>
                            <div className='text-mess'>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut iusto minima labore aperiam magni eum illo ipsum asperiores similique iure fugit tempore consequatur deleniti, id assumenda accusantium atque cupiditate! Placeat.</p>
                            </div>
                            <div className='data-wrap'>
                                <div className='data'>
                                    <svg>
                                        <use xlinkHref='#icon-calendar' />
                                    </svg>
                                    14:20
                                </div>
                            </div>
                        </div>
                        <div className='item-massage active'>
                            <div className='check'>
                                <label htmlFor="inputName" className='el-checkbox none-text'>
                                    <input type='checkbox' name='size' />
                                    <span className='checkbox-icon' />
                                </label>
                            </div>
                            <div className='img'>
                                <div className='user-pick'>
                                    <img src='/static/images/img-13.jpg' alt='' />
                                </div>
                            </div>
                            <div className='name'>
                                <a href='#' className='user-name'><span>Kathryn Garcia</span></a>
                            </div>
                            <div className='text-mess'>
                                <p>Hi) How are you?</p>
                            </div>
                            <div className='data-wrap'>
                                <div className='data'>
                                    <svg>
                                        <use xlinkHref='#icon-calendar' />
                                    </svg>
                                    Jul 25
                                </div>
                            </div>
                        </div>
                        <div className='item-massage'>
                            <div className='check'>
                                <label htmlFor="inputName" className='el-checkbox none-text'>
                                    <input type='checkbox' name='size' />
                                    <span className='checkbox-icon' />
                                </label>
                            </div>
                            <div className='img'>
                                <div className='user-pick'>
                                    <img src='/static/images/img-14.jpg' alt='' />
                                </div>
                            </div>
                            <div className='name'>
                                <a href='#' className='user-name'><span>Marie Roberts</span></a>
                            </div>
                            <div className='text-mess'>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut iusto minima labore aperiam magni eum illo ipsum asperiores similique iure fugit tempore consequatur deleniti, id assumenda accusantium atque cupiditate! Placeat.</p>
                            </div>
                            <div className='data-wrap'>
                                <div className='data'>
                                    <svg>
                                        <use xlinkHref='#icon-calendar' />
                                    </svg>
                                    Jul 25
                                </div>
                            </div>
                        </div>
                        <div className='item-massage'>
                            <div className='check'>
                                <label htmlFor="inputName" className='el-checkbox none-text'>
                                <input type='checkbox' name='size' />
                                <span className='checkbox-icon' />
                                </label>
                            </div>
                            <div className='img'>
                                <div className='user-pick'>
                                    <img src='/static/images/img-16.jpg' alt='' />
                                </div>
                            </div>
                            <div className='name'>
                                <a href='#' className='user-name'><span>Elizabeth Day</span></a>
                            </div>
                            <div className='text-mess'>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut iusto minima labore aperiam magni eum illo ipsum asperiores similique iure fugit tempore consequatur deleniti, id assumenda accusantium atque cupiditate! Placeat.</p>
                            </div>
                            <div className='data-wrap'>
                                <div className='data'>
                                    <svg>
                                        <use xlinkHref='#icon-calendar' />
                                    </svg>
                                    Jul 25
                                </div>
                            </div>
                        </div>
                        <div className='item-massage'>
                            <div className='check'>
                                <label htmlFor="inputName" className='el-checkbox none-text'>
                                    <input type='checkbox' name='size' />
                                    <span className='checkbox-icon' />
                                </label>
                            </div>
                            <div className='img'>
                                <div className='user-pick'>
                                    <img src='/static/images/img-12.jpg' alt='' />
                                </div>
                            </div>
                            <div className='name'>
                                <a href='#' className='user-name'><span>Emily Larson</span></a>
                            </div>
                            <div className='text-mess'>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut iusto minima labore aperiam magni eum illo ipsum asperiores similique iure fugit tempore consequatur deleniti, id assumenda accusantium atque cupiditate! Placeat.</p>
                            </div>
                            <div className='data-wrap'>
                                <div className='data'>
                                    <svg>
                                        <use xlinkHref='#icon-calendar' />
                                    </svg>
                                    14:20
                                </div>
                            </div>
                        </div>
                        <div className='item-massage'>
                            <div className='check'>
                                <label htmlFor="inputName" className='el-checkbox none-text'>
                                    <input type='checkbox' name='size' />
                                    <span className='checkbox-icon' />
                                </label>
                            </div>
                            <div className='img'>
                                <div className='user-pick'>
                                    <img src='/static/images/img-13.jpg' alt='' />
                                </div>
                            </div>
                            <div className='name'>
                                <a href='#' className='user-name'><span>Kathryn Garcia</span></a>
                            </div>
                            <div className='text-mess'>
                                <p>Hi) How are you?</p>
                            </div>
                            <div className='data-wrap'>
                                <div className='data'>
                                    <svg>
                                        <use xlinkHref='#icon-calendar' />
                                    </svg>
                                    Jul 25
                                </div>
                            </div>
                        </div>
                        <div className='item-massage'>
                            <div className='check'>
                                <label htmlFor="inputName" className='el-checkbox none-text'>
                                    <input type='checkbox' name='size' />
                                    <span className='checkbox-icon' />
                                </label>
                            </div>
                            <div className='img'>
                                <div className='user-pick'>
                                    <img src='/static/images/img-14.jpg' alt='' />
                                </div>
                            </div>
                            <div className='name'>
                                <a href='#' className='user-name'><span>Marie Roberts</span></a>
                            </div>
                            <div className='text-mess'>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut iusto minima labore aperiam magni eum illo ipsum asperiores similique iure fugit tempore consequatur deleniti, id assumenda accusantium atque cupiditate! Placeat.</p>
                            </div>
                            <div className='data-wrap'>
                                <div className='data'>
                                    <svg>
                                        <use xlinkHref='#icon-calendar' />
                                    </svg>
                                    Jul 25
                                </div>
                            </div>
                        </div>
                        <div className='item-massage'>
                            <div className='check'>
                                <label htmlFor="inputName" className='el-checkbox none-text'>
                                    <input type='checkbox' name='size' />
                                    <span className='checkbox-icon' />
                                </label>
                            </div>
                            <div className='img'>
                                <div className='user-pick'>
                                    <img src='/static/images/img-16.jpg' alt='' />
                                </div>
                            </div>
                            <div className='name'>
                                <a href='#' className='user-name'><span>Elizabeth Day</span></a>
                            </div>
                            <div className='text-mess'>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut iusto minima labore aperiam magni eum illo ipsum asperiores similique iure fugit tempore consequatur deleniti, id assumenda accusantium atque cupiditate! Placeat.</p>
                            </div>
                            <div className='data-wrap'>
                                <div className='data'>
                                    <svg>
                                        <use xlinkHref='#icon-calendar' />
                                    </svg>
                                    Jul 25
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}