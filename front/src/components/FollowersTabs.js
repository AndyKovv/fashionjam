import React from 'react';

export default class FollowersTabs extends React.Component {
    render() {
        return (
            <div className='castings-content mod-content follower-mod'>
                <div className='row'>
                    <ul className='col-md-12 tabs-head'>
                        <li><a href='#'>Followers <span>321</span></a></li>
                        <li><a href='#'>Following <span>56</span></a></li>
                    </ul>
                </div>
                <div className='casting-item row'>
                    <div className='col-sm-7'>
                        <div className='media-left'>
                            <div className='user-pick'>
                                <img src='/static/images/img-12.jpg' alt='' />
                            </div>
                            <div className='user'>
                                <div className='user-name'>Emily Larson</div>
                                <div className='user-prof'>Model</div>
                                <ul className='user-info'>
                                    <li><svg><use xlinkHref='#icon-location' /></svg><span>Los Angeles, USA</span></li>
                                    <li>Gender: <span>Male</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className='col-sm-5'>
                        <div className='media-right'>
                            <div className='btn-wrap vertical-center'>
                                <a href='#' className='el-btn bg-none'>Follow</a>
                                <a href='#' className='el-btn'>Message</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='casting-item row'>
                    <div className='col-sm-7'>
                        <div className='media-left'>
                            <div className='user-pick'>
                                <img src='/static/images/img-13.jpg' alt='' />
                            </div>
                            <div className='user'>
                                <div className='user-name'>Emily Larson</div>
                                <div className='user-prof'>Model</div>
                                <ul className='user-info'>
                                    <li><svg><use xlinkHref='#icon-location' /></svg><span>Kyiv, Ukraine</span></li>
                                    <li>Gender: <span>Male</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className='col-sm-5'>
                        <div className='media-right'>
                            <div className='btn-wrap vertical-center'>
                                <a href='#' className='el-btn bg-none'>Follow</a>
                                <a href='#' className='el-btn'>Message</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='casting-item row'>
                    <div className='col-sm-7'>
                        <div className='media-left'>
                            <div className='user-pick'>
                                <img src='/static/images/img-14.jpg' alt='' />
                            </div>
                            <div className='user'>
                                <div className='user-name'>Emily Larson</div>
                                <div className='user-prof'>Retoucher</div>
                                <ul className='user-info'>
                                    <li><svg><use xlinkHref='#icon-location' /></svg><span>Brenna, Czech Republic</span></li>
                                    <li>Gender: <span>Male</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className='col-sm-5'>
                        <div className='media-right'>
                            <div className='btn-wrap vertical-center'>
                                <a href='#' className='el-btn bg-none'>Follow</a>
                                <a href='#' className='el-btn'>Message</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='casting-item row'>
                    <div className='col-sm-7'>
                        <div className='media-left'>
                            <div className='user-pick'>
                                <img src='/static/images/img-16.jpg' alt='' />
                            </div>
                            <div className='user'>
                                <div className='user-name'>Emily Larson</div>
                                <div className='user-prof'>Make-up</div>
                                <ul className='user-info'>
                                    <li><svg><use xlinkHref='#icon-location' /></svg><span>Paris, France</span></li>
                                    <li>Gender: <span>Male</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className='col-sm-5'>
                        <div className='media-right'>
                            <div className='btn-wrap vertical-center'>
                                <a href='#' className='el-btn bg-none'>Follow</a>
                                <a href='#' className='el-btn'>Message</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='casting-item row'>
                    <div className='col-sm-7'>
                        <div className='media-left'>
                            <div className='user-pick'>
                                <img src='/static/images/img-12.jpg' alt='' />
                            </div>
                            <div className='user'>
                                <div className='user-name'>Emily Larson</div>
                                <div className='user-prof'>Photographer</div>
                                <ul className='user-info'>
                                    <li><svg><use xlinkHref='#icon-location' /></svg><span>Aberdeen, United Kingdom</span></li>
                                    <li>Gender: <span>Male</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className='col-sm-5'>
                        <div className='media-right'>
                            <div className='btn-wrap vertical-center'>
                                <a href='#' className='el-btn bg-none'>Follow</a>
                                <a href='#' className='el-btn'>Message</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}