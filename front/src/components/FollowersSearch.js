import React from 'react';

export default class FollowersSearch extends React.Component {
    render() {
        return (
            <div className='mod-content none-overflov'>
                <div className='row'>
                    <h2 className='title-page'>FOLLOWERS</h2>
                </div>
                <div className='row'>
                    <div className='col-md-3'>
                        <label htmlFor='type' className='el-label'>
                            Type
                            <select id='type' className='ui dropdown'>
                                <option value=''>Select</option>
                                <option value='1'>Select 1</option>
                                <option value='0'>Select 2</option>
                            </select>
                        </label>
                    </div>
                    <div className='col-md-3'>
                        <label htmlFor='country' className='el-label'>
                            Country
                            <select id='country' className='ui dropdown'>
                                <option value=''>Select</option>
                                <option value='1'>Select 1</option>
                                <option value='0'>Select 2</option>
                            </select>
                        </label>
                    </div>
                </div>
                <div className='row'>
                    <div className='col-md-2'>
                        <label htmlFor='age' className='el-label'>
                            Age
                            <select id='age' className='ui dropdown'>
                                <option value=''>From</option>
                                <option value='1'>Select 1</option>
                                <option value='0'>Select 2</option>
                            </select>
                        </label>
                    </div>
                    <div className='col-md-2 mod-mob'>
                        <label htmlFor='to' className='el-label'>
                            &nbsp;
                            <select id='to' className='ui dropdown'>
                                <option value=''>To</option>
                                <option value='1'>Select 1</option>
                                <option value='0'>Select 2</option>
                            </select>
                        </label>
                    </div>
                    <div className='col-md-6'>
                        <div className='el-label'>
                            Gender
                            <div className='radio-list'>
                                <label htmlFor='male' className='el-radio'>
                                    <input type='radio' id='male' name='size' checked />
                                    <span className='radio-icon' />
                                    Male
                                </label>
                                <label htmlFor='female' className='el-radio'>
                                    <input type='radio' id='female' name='size' />
                                    <span className='radio-icon' />
                                    Female
                                </label>
                                <label htmlFor='any' className='el-radio'>
                                    <input type='radio' id='any' name='size' />
                                    <span className='radio-icon' />
                                    Any
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}