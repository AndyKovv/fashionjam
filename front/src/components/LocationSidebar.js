import React from 'react';

export default class LocationSidebar extends React.Component {
    render() {
        return (
            <div className=''>
                <h2 className="title-page">LOCATION</h2>
                <div className="map-wrap">
                    <div id='map' />
                </div>
            </div>
        );
    }
}