import React from 'react';
import { Link } from 'react-router-dom';
import Sidebar from '../components/Sidebar';
import UserBgGllery from '../components/UserProfileBgGallery';
import AddPost from '../components/AddNewPost';
import PostWall from '../components/PostWall';


class UserProfilePage extends React.Component {
    render() {
        return (
            <div className='container'>
                <div className='row'>
                    <div className='col-md-12 content-page main-box'>
                        <Sidebar />
                        <Link to='/id1/followers'>Лінк</Link>
                        <div className='content'>
                            <UserBgGllery />
                            <AddPost />
                            <PostWall />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default UserProfilePage;