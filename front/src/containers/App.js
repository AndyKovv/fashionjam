import React from 'react';
import { bindActionCreators } from 'redux';

import { connect } from 'react-redux';
import { Route, Switch, withRouter } from 'react-router-dom';

import AppHeader from '../components/AppHeader';
import UserProfilePage from '../containers/UserProfilePage';
import MainPage from '../components/MainPage';
import Stream from '../containers/Stream';
import Messages from '../containers/Messages';
import About from '../containers/About';
import Browse from '../containers/Browse';
import CastingCalls from '../containers/CastingCalls';
import Followers from '../containers/Followers';

import * as RegisterActions from '../actions/RegisterActions';
import * as LoginActions from '../actions/UserLoginActions';

class App extends React.Component {
    componentWillMount() {
        console.log('moun');
    }
    render() {
        return (
            <div>
                <AppHeader />
                <div id='page-body' className='page-body'>
                    <Switch>
                        <Route exact path='/id:userId/' component={UserProfilePage} />
                        <Route path='/id:userId/stream/' component={Stream} />
                        <Route path='/id:userId/messages/' component={Messages} />
                        <Route path='/id:userId/about/' component={About} />
                        <Route path='/id:userId/browse/' component={Browse} />
                        <Route path='/id:userId/casting/' component={CastingCalls} />
                        <Route path='/id:userId/followers/' component={Followers} />
                        <Route path='/' render={() => <MainPage {...this.props} />} />
                    </Switch>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        register: state.register,
        user: state.user,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        registerActions: bindActionCreators(RegisterActions, dispatch),
        loginActions: bindActionCreators(LoginActions, dispatch),
    };
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));