import React from 'react';
import Sidebar from '../components/Sidebar';
import MessagesWall from '../components/MessageWall';

export default class Messages extends React.Component {
    render() {
        return (
            <div className='container'>
                <div className='row'>
                    <div className='col-md-12 content-page main-box'>
                        <Sidebar />
                        <MessagesWall />
                    </div>
                </div>
            </div>
        );
    }
}