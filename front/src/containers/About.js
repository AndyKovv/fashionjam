import React from 'react';
import UserInfoForm from '../components/UserInfoForm';
import Sidebar from '../components/Sidebar';

class About extends React.Component {
    render() {
        return (
            <div className='container'>
                <div className='row'>
                    <div className='col-md-12 content-page main-box'>
                        <Sidebar />
                        <UserInfoForm />
                    </div>
                </div>
            </div>
        );
    }
}

export default About;