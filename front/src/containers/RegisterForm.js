/* 2017 Andrii Kovalov andy.kovv@gmail.com */
/* eslint class-methods-use-this: "off" */

import React from 'react';
import $ from 'jquery';
import fancybox from 'fancybox';

const FIRST_NAME = 'firstName';
const USER_EMAIL = 'email';
const USER_PASSWORD = 'password';
const USER_SEX = 'sex';

const EMAIL_ERROR_ID_FIELD = 'email-error';
const PASSWORD_ERROR_ID_FIELD = 'password-error';
const FIRST_NAME_ERROR_ID_FIELD = 'first-name-error';

fancybox($);

export default class RegisterForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            [FIRST_NAME]: '',
            [USER_EMAIL]: '',
            [USER_PASSWORD]: '',
            [USER_SEX]: 'male',
            registerButtonDisable: false,
            emailErrors: null,
            passwordErrors: null,
            firstNameErrors: null,
        };
        this.registerUser = this.registerUser.bind(this);
        this.changeValue = this.changeValue.bind(this);
    }
    componentDidMount() {
        const that = this;

        $.fancybox({
            closeBtn: false,
            padding: 0,
            href: '#register',
            afterClose: function () {
                that.props.registerActions.modalClose();
            },
        });
    }
    componentWillReceiveProps(nextProps) {
        /* Check error in props, if exists set rigister button to enable */

        const errorData = nextProps.register.errors;
        if (errorData) {
            this.setState({
                registerButtonDisable: false,
            });
            this.parseErrors(errorData);
            return true;
        }
        return true;
    }
    componentWillUnmount() {
        $.fancybox.close();
    }
    setRegisterButtonDisable() {
        /* Procedure should disble register button on user click */
        this.setState({
            registerButtonDisable: true,
        });
        return true;
    }
    getErrorText(errorText) {
        /* Method should return error text from list */
        if (!errorText) {
            return null;
        }
        if (!errorText.length) {
            return null;
        }
        return errorText[0];
    }
    changeValue(event, key) {
        /* Procedure should change value of props */
        event.preventDefault();
        this.setState({
            [key]: event.target.value,
        });
        return true;
    }
    registerUser(e) {
        /* Functions should collect user data and dispatch to props */
        e.preventDefault();

        // Set register button to disable
        this.setRegisterButtonDisable();

        this.props.registerActions.registerUser(this.state[USER_EMAIL], this.state[FIRST_NAME], this.state[USER_PASSWORD], this.state[USER_SEX]);
        return true;
    }
    parseErrors(errorData) {
        /* Procedure should parse error and change error state */
        if (!errorData) {
            return false;
        }
        this.setState({
            emailErrors: this.getErrorText(errorData.email),
            passwordErrors: this.getErrorText(errorData.password),
            firstNameErrors: this.getErrorText(errorData.first_name),
        });
        return true;
    }
    renderError(fieldId, message) {
        /* Method should render error */
        if (!message) {
            return <div />;
        }

        // Render html element
        return (
            <div id={fieldId}>
                <p>{message}</p>
            </div>
        );
    }
    render() {
        return (
            <div className='modall-register modal' id='register'>
                <div className='title'>
                    <h2>SIGN UP WITH <b>MAIL</b> OR <b>FACEBOOK</b></h2>
                </div>
                <div className='content-modal'>
                    <form action='#'>
                        <div className='form'>
                            <div className='el-label'>
                                <input type='text' id='user-first-name' onChange={(e) => { this.changeValue(e, FIRST_NAME); }} placeholder='First Name' />
                                {this.renderError(FIRST_NAME_ERROR_ID_FIELD, this.state.firstNameErrors)}
                            </div>
                            <div className='el-label'>
                                <input type='text' id='user-email' placeholder='Email' onChange={(e) => { this.changeValue(e, USER_EMAIL); }} />
                                {this.renderError(EMAIL_ERROR_ID_FIELD, this.state.emailErrors)}
                            </div>
                            <div className='el-label'>
                                <input type='password' id='user-password' placeholder='Password' onChange={(e) => { this.changeValue(e, USER_PASSWORD); }} />
                                {this.renderError(PASSWORD_ERROR_ID_FIELD, this.state.passwordErrors)}
                            </div>
                            <label htmlFor='genderChose' className='el-label'>
                                <select id='genderChose' value='male' className='ui dropdown select-dropdown' onChange={(e) => { this.changeValue(e, USER_SEX); }} >
                                    <option value='male'>Male</option>
                                    <option value='female'>Female</option>
                                </select>
                            </label>
                            {/* TO DO
                            <i className="dropdown icon"></i>
                            <div className="default text">Male</div>
                                <div className="menu transition visible" tabindex="-1" style="display: block !important;">
                                <div className="item" data-value="1">Select 1</div><div className="item" data-value="0">Select 2</div>
                            </div>
                            */}
                            <p className='terms'>By signing up you agree our <a href='#'>Terms of Service</a> and <a href='#'>Privacy Policy</a></p>
                            <button id='sign-up' className='el-btn' onClick={this.registerUser} disabled={this.state.registerButtonDisable}>SIGN UP</button>
                            <p className='or'>OR</p>
                            <button className='el-btn mod-fb'><img src='../static/images/fb.png' alt='' /> Continue with FACEBOOK</button>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}
