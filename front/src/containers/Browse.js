import React from 'react';
import AdvancedSearchSidebar from '../components/AdvancedSearchSidebar';
import SearchResult from '../components/SearchResult';

export default class Browse extends React.Component {
    render() {
        return (
            <div className='container'>
                <div className='row'>
                    <div className='col-md-12 content-page main-box'>
                        <AdvancedSearchSidebar />
                        <SearchResult />
                    </div>
                </div>
            </div>
        );
    }
}