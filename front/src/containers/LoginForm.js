/*  globals $:true */

import React from 'react';

export default class LoginForm extends React.Component {
    componentDidMount() {
        const that = this;
        $.fancybox({
            closeBtn: false,
            padding: 0,
            href: '#login',
            afterClose: function () {
                that.props.history.push('/');
            },
        });
    }
    componentWillUnmount() {
        $.fancybox.close();
    }
    render() {
        return (
            <div className='modall-register modal' id='login'>
                <div className='title registration-form__title'>
                    <h2>LOG IN</h2>
                </div>
                <div className='content-modal registration-modal'>
                    <form action='#'>
                        <div className='modall-register__label'>Not a member? Create an accout</div>
                        <button className='el-btn mod-fb'><img src='../static/images/fb.png' alt='' />Continue with FACEBOOK</button>
                        <div className='modall-register__label'>Or login below:</div>
                        <div className='form'>
                            <div className='el-label'>
                                <input type='text' placeholder='Email' />
                            </div>
                            <div className='el-label'>
                                <input type='password' placeholder='Password' />
                            </div>
                            <button className='el-btn'>LOG IN</button>
                            <a href='' className='register__label'>Forgot password?</a>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}
