import React from 'react';
import StreamPhotoWall from '../components/StreamPhotoWall';
import PostFilter from '../components/PostFilter';
import StreamSidebar from '../components/StreamSidebar';
import PostWall from '../components/PostWall';

class Stream extends React.Component {
    render() {
        return (
            <div className='container'>
                <div className='row'>
                    <div className='col-md-12'>
                        <StreamPhotoWall />
                    </div>
                    <div className='col-md-12 content-page main-box mod-right'>
                        <div className='content posts-wall'>
                            <PostFilter />
                            <PostWall />
                        </div>
                        <StreamSidebar />
                    </div>
                </div>
            </div>
        );
    }
}

export default Stream;