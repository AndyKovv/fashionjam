import React from 'react';
import Sidebar from '../components/Sidebar';
import FollowersSearch from '../components/FollowersSearch';
import SearchInput from '../components/SearchInput';
import FollowersTabs from '../components/FollowersTabs';

export default class Followers extends React.Component {
    render() {
        return (
            <div className='container'>
                <div className='row'>
                    <div className='col-md-12 content-page main-box'>
                        <Sidebar />
                        <div className='content'>
                            <FollowersSearch />
                            <SearchInput />
                            <FollowersTabs />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}