import React from 'react';
import AdvancedSearchSidebar from '../components/AdvancedSearchSidebar';
import LocationSidebar from '../components/LocationSidebar';
import SearchInput from '../components/SearchInput';
import CastingSearchResult from '../components/CastingSearchResult';
import CastingCallsSelect from '../components/CastingCallsSelect';

export default class CastingCalls extends React.Component {
    render() {
        return (
            <div className='container'>
                <div className='row'>
                    <div className='col-md-12 content-page main-box'>
                        <div className='sidebar search-sidebar'>
                            <LocationSidebar />
                            <AdvancedSearchSidebar />
                        </div>
                        <div className='content'>
                            <CastingCallsSelect />
                            <SearchInput />
                            <CastingSearchResult />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}