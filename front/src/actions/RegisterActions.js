/* globals localStorage:true fetch:true  */

import { push } from 'react-router-redux';

import { checkHttpStatus, parseJSON } from '../utils';

import {
    REGISTER_USER_OPEN_MODAL,
    REGISTER_USER_CLOSE_MODAL,
    REGISTER_USER_REQUEST,
    REGISTER_USER_SUCCESS,
    REGISTER_USER_FAILURE,
} from '../constants/RegisterUser';

import {
    USER_REGISTER_URL,
} from '../constants/Urls';

import {
    AUTH_TOKEN_KEY,
} from '../constants/AppConstants';


export function registerUser(email, first_name, password, sex) {
    return (dispatch) => {
        dispatch({
            type: REGISTER_USER_REQUEST,
        });
        return fetch(USER_REGISTER_URL, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            credentials: 'include',
            body: JSON.stringify({
                email, first_name, password, sex,
            }),
        })
            .then(checkHttpStatus)
            .then(parseJSON)
            .then((response) => {
                dispatch(registerUserSuccess(response));
                const location = `/id${response.user_id}/`;
                dispatch(push(location));
            })
            .catch((error) => {
                // send errror
                error.then((data) => {
                    dispatch(registerUserFailure(data));
                });
            });
    };
}


export function registerUserSuccess(response) {
    localStorage.setItem(AUTH_TOKEN_KEY, response.token);
    return {
        type: REGISTER_USER_SUCCESS,
        payload: response,
    };
}

export function registerUserFailure(response) {
    return {
        type: REGISTER_USER_FAILURE,
        payload: response,
    };
}

export function modalClose() {
    return (dispatch) => {
        dispatch({ type: REGISTER_USER_CLOSE_MODAL });
    };
}

export function modalOpen() {
    return (dispatch) => {
        dispatch({ type: REGISTER_USER_OPEN_MODAL });
    };
}
