import fetch from 'isomorphic-fetch';

import { checkHttpStatus, parseJSON } from '../utils';

import {
    LOGIN_USER_REQUEST,
    LOGIN_USER_SUCCESS,
    LOGIN_USER_FAIL,
} from '../constants/LoginUser';

import {
    USER_LOGIN_URL,
} from '../constants/Urls';


export function loginUser(email, password) {
    /* Fuction for login user */
    return (dispatch) => {
        dispatch({
            type: LOGIN_USER_REQUEST,
        });
        return fetch(USER_LOGIN_URL, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            credentials: 'include',
            body: JSON.stringify({
                email, password,
            }),
        })
            .then(checkHttpStatus)
            .then(parseJSON)
            .then((response) => {
                dispatch(loginUserSuccess(response));
            })
            .catch((error) => {
                dispatch(loginUserFail(error));
            });
    };
}


export function loginUserSuccess(response) {
    return {
        type: LOGIN_USER_SUCCESS,
        payload: response,
    };
}

export function loginUserFail(error) {
    return {
        type: LOGIN_USER_FAIL,
        payload: error,
    };
}