/* globals document:true sessionStorage: true */
/* File consist from custom project utils */

import {
    AUTH_TOKEN_KEY,
} from '../constants/AppConstants';


export function checkHttpStatus(response) {
    if (response.status >= 200 && response.status < 300) {
        return response;
    }
    return Promise.reject(parseJSON(response));
}

export function parseJSON(response) {
    return response.json();
}

export function getCSRF() {
    return document.getElementsByName('csrfmiddlewaretoken')[0].defaultValue;
}

export function getAuthToken() {
    /* Function should return authtoken from local storage  */
    const token = sessionStorage.getItem(AUTH_TOKEN_KEY);
    if (!token) {
        return null;
    }
    return token;
}
