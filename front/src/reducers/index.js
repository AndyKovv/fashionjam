import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import registerReducer from './registerReducer';
import userReducer from './userReducer';


export const rootReducer = combineReducers({
    routing: routerReducer,
    user: userReducer,
    register: registerReducer,
});