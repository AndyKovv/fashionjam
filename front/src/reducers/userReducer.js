
import {
    LOGIN_USER_REQUEST,
    LOGIN_USER_SUCCESS,
    LOGIN_USER_FAIL,
} from '../constants/LoginUser';

const initialState = {
    isAutheticated: false,
    fetching: false,
    user: {},
    errors: {},
};

export default function loginUser(state = initialState, action) {
    switch (action.type) {
    case LOGIN_USER_REQUEST:
        return {
            ...state, fetching: true,
        };
    case LOGIN_USER_SUCCESS:
        return {
            ...state, isAutheticated: true, fetching: false, user: action.payload,
        };
    case LOGIN_USER_FAIL:
        return {
            ...state, isAutheticated: false, fetching: false, errors: action.payload,
        };
    default:
        return state;
    }
}
