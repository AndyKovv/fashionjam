import {
    REGISTER_USER_OPEN_MODAL,
    REGISTER_USER_CLOSE_MODAL,
    REGISTER_USER_REQUEST,
    REGISTER_USER_FAILURE,
    REGISTER_USER_SUCCESS,
} from '../constants/RegisterUser';

const initialState = {
    fetching: false,
    openModal: false,
    errors: {},
    payload: {},
};

export default function registerUser(state = initialState, action) {
    switch (action.type) {
    case REGISTER_USER_REQUEST:
        return { ...state, fetching: true };
    case REGISTER_USER_OPEN_MODAL:
        return { ...state, openModal: true };
    case REGISTER_USER_CLOSE_MODAL:
        return { ...state, openModal: false };
    case REGISTER_USER_SUCCESS:
        return { ...state, fetching: false, payload: action.payload };
    case REGISTER_USER_FAILURE:
        return { ...state, fetching: false, errors: action.payload.error };
    default:
        return state;
    }
}