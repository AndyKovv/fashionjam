const webpack = require('webpack');
const BundleTracker = require('webpack-bundle-tracker');
require('babel-polyfill');

const config = require('./webpack.base.config.js');

config.entry = [
    'babel-polyfill',
    'webpack-dev-server/client?http://localhost:3000',
    'webpack/hot/only-dev-server',
    './src/index',
];

config.watch = true;

config.output.publicPath = 'http://localhost:3000/dist/';

config.plugins = config.plugins.concat([
    new webpack.HotModuleReplacementPlugin(),
    new BundleTracker({ filename: './dist/webpack-stats.json' }),
]);

config.devtool = 'source-map';
module.exports = config;
