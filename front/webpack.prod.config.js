const webpack = require('webpack');
const BundleTracker = require('webpack-bundle-tracker');
require('babel-polyfill');
const config = require('./webpack.base.config.js');

config.output.path = require('path').resolve('./dist');
console.log(config.output.path)  // eslint-disable-line

config.plugins = config.plugins.concat([
    new BundleTracker({ filename: './dist/webpack-stats-prod.json' }),
  // removes a lot of debugging code in React
    new webpack.DefinePlugin({
        'process.env': {
            'NODE_ENV': JSON.stringify('production'),
        },
    }),
    // keeps hashes consistent between compilations
    new webpack.optimize.OccurrenceOrderPlugin(),
    // minifies code
    new webpack.optimize.UglifyJsPlugin({
        compress: {
            warnings: false,
            drop_console: true,
            unsafe: true,
        },
    }),
]);

module.exports = config;

