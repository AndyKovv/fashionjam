/* 2017 Andrii Kovalov andy.kovv@gmail.com */

import sinon from 'synon';

export const mockPreventDefaultEvent = { 'preventDefault': sinon.spy() }

export function mockInputValue(value) {
    return {
        preventDefault: sinon.spy(),
        target: {
            value: value
        }
    }
}
