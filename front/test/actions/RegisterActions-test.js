/* 2017 Andrii Kovalov andy.kovv@gmail.com */

import thunk from 'redux-thunk';
import configureMockStore from 'redux-mock-store';

import fetchMock from 'fetch-mock';
import expect from 'expect';


import * as actions from '../../src/actions/RegisterActions';
import * as constants from '../../src/constants/RegisterUser';
import * as urls from '../../src/constants/Urls';
import * as appConst from '../../src/constants/AppConstants';


const middleware = [thunk]
const mockStore = configureMockStore(middleware)

describe('>>> REGISTER ACTIONS ', () => {
    const response = {
        user_id: 1,
        token: 'someT0ken'
    }
    const errorResponse = {
        error: {
          email: ["Not a valid email address."],
          password: ["ERROR_PASSWORD_STREIGHT"],
          sex: ["ERROR_SEX_VALUE"]
        }
    }
    const email = 'someEmail@m.com'
    const first_name = 'firstName'
    const password = 'Password'
    const sex = 'uknown'

    afterEach(() => {
        fetchMock.restore();
    });

    it('should success register user and push to history url and save to local storage', () => {
        fetchMock.postOnce('/api-auth/v1/register/', response);

        const expectedActions = [
            { type: constants.REGISTER_USER_REQUEST},
            { type: constants.REGISTER_USER_SUCCESS, payload: response},
            { payload: {args: ["/id1/"], method: "push"}, type: "@@router/CALL_HISTORY_METHOD"}
        ]
        const store = mockStore({
            fetching: false,
            payload: {}
        });

        return store.dispatch(actions.registerUser(
            email, first_name, password, sex
        )).then(() => {
            const userToken = localStorage.getItem(appConst.AUTH_TOKEN_KEY);
            expect(userToken).toEqual(response.token);
            expect(store.getActions()).toEqual(expectedActions);
        });
    });

    it('should failed register user ', () => {
        fetchMock.mock('/api-auth/v1/register/', {
            body: errorResponse,
            status:400,
            sendAsJson: true,
        });
        const expectedActions = [
            { type: constants.REGISTER_USER_REQUEST },
            { type: constants.REGISTER_USER_FAILURE, payload: errorResponse }
        ]
        const store = mockStore({
            fetching: false,
            payload: {}
        });
        return store.dispatch(actions.registerUser(
            email, first_name, password, sex
        )).then(() => {
            expect(store.getActions()).toEqual(expectedActions);
        })
    });
});