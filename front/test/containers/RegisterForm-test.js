/* 2017 Andrii Kovalov andy.kovv@gmail.com */

import React from 'react';
import {shallow } from 'enzyme';
import sinon from 'sinon';
import { expect, assert } from 'chai';

import RegisterFrom from '../../src/containers/RegisterForm';

function setUp() {
    const props = {
        registerActions: {
            registerUser: sinon.spy(),
            modalClose: sinon.spy,
        }
    }
    return props;
}
const mockEvent = { preventDefault: sinon.spy() }

const mockInputValue = {
    preventDefault: sinon.spy(),
    target: {
        value: 'andy'
    }
} 

function setInputValue(value) {
    mockInputValue.target.value = value;
}

const mockedErrorsFromServer = {
    errors:{
        'email': ['Email is invalid'],
        'password': ['Not to strong'],
        'first_name': ['Is required'],
    }
}

describe('>>>> REGISTER USER FORM', () => {
    let registerFrom, props;

    beforeEach(() => {
        props = setUp();
        registerFrom = shallow(<RegisterFrom {...props} />);
    });
    it('test should check send params on user register', () => {
        registerFrom.find('#sign-up').simulate('click', mockEvent);
        sinon.assert.calledOnce(mockEvent.preventDefault);
        sinon.assert.calledOnce(props.registerActions.registerUser)
    });

    it('test should check field values on change', () => {
        let state = registerFrom.instance().state;
        
        // Check default state
        assert.equal(state['firstName'], '');
        assert.equal(state['email'], '');
        assert.equal(state['password'], '');
        assert.equal(state['sex'], 'male');

        // Check state on change fields
        registerFrom.find('#user-first-name').simulate('change', mockInputValue, 'firstName');
        state = registerFrom.instance().state;
        assert.equal(state['firstName'], 'andy');

        setInputValue('email');
        registerFrom.find('#user-email').simulate('change', mockInputValue, 'email');
        state = registerFrom.instance().state;
        assert.equal(state['email'], 'email');

        setInputValue('somePassword');
        registerFrom.find('#user-password').simulate('change', mockInputValue, 'somePassword');
        state = registerFrom.instance().state;
        assert.equal(state['password'], 'somePassword');

        setInputValue('female');
        registerFrom.find('#genderChose').simulate('change', mockInputValue, 'sex')
        state = registerFrom.instance().state;
        assert.equal(state['sex'], 'female');
    });

    it('test should check button disable on register user', () => {
        /* Test chack register button disable on register user */
        let registerButton = registerFrom.find('#sign-up');

        assert.equal(registerButton.prop('disabled'), false);

        registerButton.simulate('click', mockEvent);

        registerButton = registerFrom.find('#sign-up');
        assert.equal(registerButton.prop('disabled'), true);
        props.register = {
            errors: {'password': ['User register']}
        }

        registerFrom.setProps(props);
        registerButton = registerFrom.find('#sign-up');
        assert.equal(registerButton.prop('disabled'), false);
    });

    it('test should show error messages on user register', () => {
        /* Test should check error text and message */
        let registerButton = registerFrom.find('#sign-up').simulate('click', mockEvent);
        props.register = mockedErrorsFromServer

        registerFrom.setProps(props);

        const emailError = registerFrom.find('#email-error');
        assert.equal(emailError.length, 1);
        assert.equal(emailError.text(), mockedErrorsFromServer.errors.email);

        const firstNamError = registerFrom.find('#first-name-error');
        assert.equal(firstNamError.length, 1);
        assert.equal(firstNamError.text(), mockedErrorsFromServer.errors.first_name)

        const passwordError = registerFrom.find('#password-error');
        assert.equal(passwordError.length, 1);
        assert.equal(passwordError.text(), mockedErrorsFromServer.errors.password);
    });
});
