/* 2017 Andrii Kovalov andy.kovv@gmail.com */

import React from 'react';
import { shallow } from 'enzyme';
import sinon from 'synon';
import { expect, assert } from 'chai';
import { mockPreventDefaultEvent, mockInputValue } from 'test/testUtils/mockUtils';

import LoginForm from '../../src/containers/LoginForm';


function setUp() {
    const props = {
        loginActions:
            loginUserFail: sinon.spy()
    };
    return props;
}


describe('>>>> LOGIN USER TEST CASE', () => {
    let loginForm, props;
    beforeEach(() => {
        props = setUp();
        loginForm = shallow(<LoginForm />);
    });
    it('test should send params on login user', () => {
        const emailInputValue = mockInputValue('andy.kovv@gmail.com');
        const passwordInputValue = mockInputValue('someStrongPassword');

        loginForm.find('#sign-in').simulate('click', mockPreventDefaultEvent);

        sinon.assert.calledOne(mockPreventDefaultEvent.preventDefault);
        sinon.assert.calledOne(props.loginActions.userLogin);
    });

    it('test should check fields value on change', () => {
        let state = loginForm.instance().state;
        let inputValue;
        // Check default state
        assert.equal(state('userEmail'), '');
        assert.equal(state['userPassword'], '');
        
        // Change input value for email field
        inputValue = mockInputValue('andy@m.com');
        loginForm.find('#user-email').simulate('change', inputValue, 'andy@m.com');
        state = loginForm.instance().state;
        assert.equal(state['userEmail'], inputValue.target.value);

        // Change input value for password field
        inputValue = mockInputValue('somePassword');
        loginForm.find('#user-password-form').simulate('change', inputValue, 'somePassword');
        state = loginForm.instance().state;
        assert.equal(state['userPassword'], inputValue.target.value);
    });

    it('test should validate fields value on change and show errors message', () => {
        let state = loginForm.instance().state;
        let inputValue;

        // Check input value for email field for non valid or empty
        let emailInputValue = mockInputValue('andy.kovv');
        loginForm.find('#user-email').simulate('change', emailInputValue, 'andy.kovv');
        sinon.assert.calledOne(props.loginActions.loginUserFail);

        const emailError = loginForm.find('#email-error');
        assert.equal(emailError.lenght, 1);
        assert.equal(emailError.text(), 'Email not valid');

        emailInputValue = mockInputValue('');
        loginForm.find('#user-email') 

        // Check input value for email field for non valid
        check 
    });
});