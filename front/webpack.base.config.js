const path = require('path')
const webpack = require('webpack')
require('babel-polyfill')

const project_dir = path.resolve('./src')

module.exports = {
  context: __dirname,
  entry: [
    'babel-polyfill',
    './src/index',
  ],
  output: {
    path: path.resolve('./dist/'),
    //path: __dirname + '/assets/dist',
    filename: '[name].js',
  },
  plugins: [
    new webpack.NoEmitOnErrorsPlugin(),
  ],
  module: {
    loaders: [
      { test: /\.js$/, exclude: /node_modules/, loaders: ['react-hot-loader/webpack', 'babel-loader', 'eslint-loader'] },
      { test: /\.css$/, loader: 'style-loader!css-loader' }
    ]
  },
  resolve: {
    modules: [
      project_dir,
      'node_modules'
    ],
    extensions: ['.js']
  },
  resolveLoader: {
    moduleExtensions: ['*-loader', '*'],
    extensions: ['.js']
  },
}
