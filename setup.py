from setuptools import find_packages, setup

install_requires = ['aiohttp',
                    'aiopg[sa]',
                    'aiohttp-jinja2',
                    'trafaret-config']


setup(name='aiohttp-fashionjam',
      version='0.0.1',
      description='Fashion Social Network',
      platforms=['POSIX'],
      packages=find_packages(),
      package_data={
          '': ['templates/*.html', 'static/*.*']
      },
      include_package_data=True,
      install_requires=install_requires,
      setup_requires=['pytest-runner'],
      tests_require=['pytest'],
      zip_safe=False)
