# 2017 Andrii Kovalov andy.kovv@gmail.com

import aioredis


async def init_redis(app):
    """
        Procedure for init redis connection pool
    """
    conf = app['config']['redis']
    engine = await aioredis.create_redis_pool(
        (conf['host'], conf['port']),
        db=conf['database'],
        password=conf['password'],
        encoding=conf['encoding'],
        minsize=conf['min_pool_size'],
        maxsize=conf['max_pool_size'],
        loop=app.loop
    )
    app['redis'] = engine

async def close_redis(app):
    """ Procedrure should close connection """
    app['redis'].close()
    await app['redis'].wait_closed()
