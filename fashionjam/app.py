import os
import aiohttp_jinja2
# import aiohttp_debugtoolbar
import jinja2

from aiohttp import web
from aiojobs.aiohttp import setup
from trafaret_config import read_and_validate
from main_page_api.routes import routes
from auth_service.routes import routes as auth_routes
from core.middlewares.error_middleware import error_middleware
from core.middlewares.middleware_404 import middleware_404

from core.db_connection import init_pg, close_pg, Postgres
from redis_db import init_redis, close_redis
from config.utils import TRAFARET


def init_app():
    config = read_and_validate('./config/fashion_jam.yaml', TRAFARET)
    if os.environ.get('TEST_CONFIG'):
        config = read_and_validate('./config/fashion_jam_test.yaml', TRAFARET)

    app = web.Application(middlewares=[error_middleware, middleware_404])
    # aiohttp_debugtoolbar.setup(app)
    setup(app)
    app['config'] = config
    app['react_url'] = '{}{}'.format(config['webpack']['path'], config['webpack']['file'])
    aiohttp_jinja2.setup(
        app,
        loader=jinja2.FileSystemLoader('fashionjam/templates')
    )
    new_routes = routes + auth_routes
    for route in new_routes:
        app.router.add_route(route[0], route[1], route[2], name=route[3])

    app.router.add_static('/static', 'fashionjam/static', name='static')

    # init db and close connction on stop server
    postgres = Postgres(app.loop, app['config'])

    # app.on_startup.append(init_pg)
    app.on_startup.append(postgres.connect)
    app.on_startup.append(init_redis)

    app.on_cleanup.append(close_pg)
    app.on_cleanup.append(close_redis)

    return app
