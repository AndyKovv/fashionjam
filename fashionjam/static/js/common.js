'use strict';

// sticky footer
//-----------------------------------------------------------------------------------
if (!Modernizr.flexbox) {
    (function() {
        var $pageWrapper = $('#page-wrapper'),
            $pageBody = $('#page-body'),
            noFlexboxStickyFooter = function() {
                $pageBody.height('auto');
                if ($pageBody.height() + $('#header').outerHeight() + $('#footer').outerHeight() < $(window).height()) {
                    $pageBody.height($(window).height() - $('#header').outerHeight() - $('#footer').outerHeight());
                } else {
                    $pageWrapper.height('auto');
                }
            };
        $(window).on('load resize', noFlexboxStickyFooter);
    })();
}
if (ieDetector.ieVersion == 10 || ieDetector.ieVersion == 11) {
    (function() {
        var $pageWrapper = $('#page-wrapper'),
            $pageBody = $('#page-body'),
            ieFlexboxFix = function() {
                if ($pageBody.addClass('flex-none').height() + $('#header').outerHeight() + $('#footer').outerHeight() < $(window).height()) {
                    $pageWrapper.height($(window).height());
                    $pageBody.removeClass('flex-none');
                } else {
                    $pageWrapper.height('auto');
                }
            };
        ieFlexboxFix();
        $(window).on('load resize', ieFlexboxFix);
    })();
}

// placeholder
//-----------------------------------------------------------------------------------
$(function() {
    $('input[placeholder], textarea[placeholder]').placeholder();
});

// fixed svg show
//-----------------------------------------------------------------------------
function fixedSvg() {
    var baseUrl = window.location.protocol + '//' + window.location.host + window.location.pathname + window.location.search;
    $('use').filter(function() {
        return ($(this).attr("xlink:href").indexOf("#") > -1);
    }).each(function() {
        $(this).attr("xlink:href", baseUrl + $(this).attr("xlink:href").split('/').slice(-1)[0]);
    });
}

fixedSvg();


// checking if element for page
//-----------------------------------------------------------------------------------
function isOnPage(selector) {
    return ($(selector).length) ? $(selector) : false;
}

$('.ui.dropdown').dropdown();

function heightImg(itemImg) {
    itemImg.each(function() {
        var heightItem = $(this).outerWidth();
        $(this).css('height', heightItem);
    });
}
var heightImgPor = $('.item-portfolio').outerWidth();

function heightImgPortfolio(itemImg) {
    itemImg.each(function() {
        $(this).css('height', heightImgPor);
    });
}

function img_bacgraund(wrap_img) {
    wrap_img.each(function() {
        var img_src = $(this).find("img").attr('src');
        $(this).css('background-image', 'url(' + img_src + ')');
    });
}

$(window).on('debounce', function(event) {
    heightImg($('.colection-item'));
    heightImgPortfolio($('.item-portfolio'));
});

heightImg($('.colection-item'));
heightImgPortfolio($('.item-portfolio'));

img_bacgraund($('.colection-item'));
img_bacgraund($('.item-portfolio .img-wrap'));
img_bacgraund($('.item-video .img-wrap'));



if (isOnPage('.js-posts')) {
    $(window).on('load debounce', function() {
        if ($(window).width() > 767) {
            setTimeout(function() {
                $('.js-posts').masonry({
                    itemSelector: '.item-post-wrap',
                    columnWidth: '.grid-sizer',
                    percentPosition: true,
                    isAnimated: true
                });
            }, 50);

        }
    });
}
// $(window).on('load debounce', function() {
$(document).on('click', '.more', function(event) {
    if ($(window).width() < 1230) {

        event.preventDefault();
        /* Act on the event */
        if ($('.overflow').hasClass('js-active')) {
            $('body').removeAttr('style');
        } else {
            $('body').css('overflow', 'hidden');
        }

        $('.sidebar').toggleClass('js-active');
        $('.overflow').toggleClass('js-active');


    }
});

$(document).on('click', '.burger', function(event) {
  $('.js-mob-nav').toggleClass('js-active');
});

// });


$(document).on('click', '.overflow', function(event) {
    event.preventDefault();
    $('body').removeAttr('style');
    $('.sidebar').removeClass('js-active');
    $('.overflow').removeClass('js-active');

});


if (isOnPage(".item-portfolio")) {
    $('.item-portfolio').on('click', function(event) {
        event.preventDefault();
        $.ajax({
            type: "POST",
            cache: false,
            url: this.href, // preview.php
            data: 'id=' + $(this).attr('data-id'),
            success: function(data) {
                $.fancybox(data, {
                    'closeBtn': false
                });
            }
        });
    });

}

if (isOnPage(".item-video")) {
    $('.item-video').on('click', function(event) {
        event.preventDefault();
        $.ajax({
            type: "POST",
            cache: false,
            url: this.href, // preview.php
            data: 'id=' + $(this).attr('data-id'),
            success: function(data) {
                $.fancybox(data, {
                    'closeBtn': false
                });
            }
        });
    });
}


$('.js-modal-casting').fancybox({
    'closeBtn': false
});

$('.js-modal-link').fancybox({
    'closeBtn': false,
    'padding': 0
});

$('.login-modal').fancybox({
    'closeBtn': false,
    'padding': 0
});


// casting form gallery

$('.list-photos').owlCarousel({
    loop: true,
    animateOut: 'fadeOut',
    mouseDrag: 'false',
    autoplayHoverPause: true,
    margin: 0,
    nav: true,
    items: 3,
    navText: ["",""]
});

$('.home-slider').owlCarousel({
    loop: true,
    animateOut: 'fadeOut',
    mouseDrag: 'false',
    autoplay: true,
    autoplayTimeout: 1000,
    autoplayHoverPause: true,
    margin: 0,
    nav: true,
    items: 1,
    navText: ["",""]
});



// $('.list-photos  li  a').magnificPopup({
//     type: 'image',
//     gallery: {
//         enabled: true,
//         tCounter: ''
//     }
// });