run:
	export PYTHONPATH=$PYTHONPATH:/var/www/fashionjam/ && python main.py

migrate:
	export PYTHONPATH=$PYTHONPATH:/var/www/fashionjam/ && alembic revision --autogenerate -m "${message}"

upgrade:
	export PYTHONPATH=$PYTHONPATH:/var/www/fashionjam/ && alembic upgrade head

watch:
	cd front && npm run watch

build_prod:
	cd front && npm run build

npm_install:
	cd front && npm install

test:
	export PYTHONPATH=$PYTHONPATH:/var/www/fashionjam/ \
	&& TEST_CONFIG=true alembic upgrade head && \
	python setup.py test

watch_test:
	@ if [[ ! -z "${test}" ]]; then \
		export PYTHONPATH=$PYTHONPATH:/var/www/fashionjam/ && \
		TEST_CONFIG=true alembic upgrade head && \
		TEST_CONFIG=true ptw --runner "pytest ${test}"; \
	fi

