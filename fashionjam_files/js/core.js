// custom jQuery validation
//-----------------------------------------------------------------------------------
var validator = {
  init:          function () {
    $('form').each(function () {
      var name = $(this).attr('name');
      if (valitatorRules.hasOwnProperty(name)) {
        var rules = valitatorRules[name];
        $(this).validate({
          rules:          rules,
          errorElement:   'b',
          errorClass:     'error',
          focusInvalid:   true,
          focusCleanup:   false,
          errorPlacement: function (error, element) {
            validator.setError($(element), error);
          },
          highlight:      function (element, errorClass, validClass) {
            var $el     = validator.defineElement($(element)),
                $elWrap = $el.closest('.el-field');
            if ($el) {
              $el.removeClass(validClass).addClass(errorClass);
              $elWrap.removeClass('show-check');
            }
          },
          unhighlight:    function (element, errorClass, validClass) {
            var $el     = validator.defineElement($(element)),
                $elWrap = $el.closest('.el-field');
            if ($el) {
              $el.removeClass(errorClass).addClass(validClass);
              if ($elWrap.hasClass('check-valid')) {
                $elWrap.addClass('show-check');
              }
              $el.closest('el-field').addClass('show-check')
            }
          },
          messages: {
            'user_email': {
              required: 'Введите',
              email: 'Валид'
            },
            'user_name': {
              required: 'Введите',
              letters: 'Валид'
            }
          }
        });
      }
    });
  },
  setError:      function ($el, message) {
    $el = this.defineElement($el);
    if ($el) this.domWorker.error($el, message);
  },
  defineElement: function ($el) {
    return $el;
  },
  domWorker:     {
    error: function ($el, message) {
      if ($el.attr('type') == 'file') $el.parent().addClass('file-error');
      $el.addClass('error');
      $el.after(message);
    }
  }
};


// rule for form namespace
//-----------------------------------------------------------------------------------
var valitatorRules = {
  'form_subscription': {
    'user_email': {
      required: true,
      email: true
    }
  },

  'form_oneClick': {
    'user_name': {
      required: true,
      letters: true
    },
    'user_email': {
      required: true,
      email: true
    },
    'user_phone': {
      required: true,
      digits: true
    }
  },

  'form_login': {
    'user_login': {
      required: true
    },
    'user_password': {
      required: true,
      minlength: 6
    }
  },

  'form_registry': {
    'user_email': {
      required: true,
      email: true
    },
    'user_password': {
      required: true,
      minlength: 6
    },
    'user_password_confirm': {
      required: true,
      minlength: 6,
      equalTo: "#user_password"
    }
  }
};

// custom rules
//-----------------------------------------------------------------------------------
$.validator.addMethod("email", function (value) {
  if (value == '') return true;
  var regexp = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
  return regexp.test(value);
});

$.validator.addMethod("letters", function (value, element) {
  return this.optional(element) || /^[^1-9!@#\$%\^&\*\(\)\[\]:;,.?=+_<>`~\\\/"]+$/i.test(value);
});

$.validator.addMethod("digits", function (value, element) {
  return this.optional(element) || /^(\+?\d+)?\s*(\(\d+\))?[\s-]*([\d-]*)$/i.test(value);
});

//  validator init
//-----------------------------------------------------------------------------------
validator.init();

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImN1c3RvbS52YWxpZGF0ZS5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJjb3JlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLy8gY3VzdG9tIGpRdWVyeSB2YWxpZGF0aW9uXG4vLy0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG52YXIgdmFsaWRhdG9yID0ge1xuICBpbml0OiAgICAgICAgICBmdW5jdGlvbiAoKSB7XG4gICAgJCgnZm9ybScpLmVhY2goZnVuY3Rpb24gKCkge1xuICAgICAgdmFyIG5hbWUgPSAkKHRoaXMpLmF0dHIoJ25hbWUnKTtcbiAgICAgIGlmICh2YWxpdGF0b3JSdWxlcy5oYXNPd25Qcm9wZXJ0eShuYW1lKSkge1xuICAgICAgICB2YXIgcnVsZXMgPSB2YWxpdGF0b3JSdWxlc1tuYW1lXTtcbiAgICAgICAgJCh0aGlzKS52YWxpZGF0ZSh7XG4gICAgICAgICAgcnVsZXM6ICAgICAgICAgIHJ1bGVzLFxuICAgICAgICAgIGVycm9yRWxlbWVudDogICAnYicsXG4gICAgICAgICAgZXJyb3JDbGFzczogICAgICdlcnJvcicsXG4gICAgICAgICAgZm9jdXNJbnZhbGlkOiAgIHRydWUsXG4gICAgICAgICAgZm9jdXNDbGVhbnVwOiAgIGZhbHNlLFxuICAgICAgICAgIGVycm9yUGxhY2VtZW50OiBmdW5jdGlvbiAoZXJyb3IsIGVsZW1lbnQpIHtcbiAgICAgICAgICAgIHZhbGlkYXRvci5zZXRFcnJvcigkKGVsZW1lbnQpLCBlcnJvcik7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBoaWdobGlnaHQ6ICAgICAgZnVuY3Rpb24gKGVsZW1lbnQsIGVycm9yQ2xhc3MsIHZhbGlkQ2xhc3MpIHtcbiAgICAgICAgICAgIHZhciAkZWwgICAgID0gdmFsaWRhdG9yLmRlZmluZUVsZW1lbnQoJChlbGVtZW50KSksXG4gICAgICAgICAgICAgICAgJGVsV3JhcCA9ICRlbC5jbG9zZXN0KCcuZWwtZmllbGQnKTtcbiAgICAgICAgICAgIGlmICgkZWwpIHtcbiAgICAgICAgICAgICAgJGVsLnJlbW92ZUNsYXNzKHZhbGlkQ2xhc3MpLmFkZENsYXNzKGVycm9yQ2xhc3MpO1xuICAgICAgICAgICAgICAkZWxXcmFwLnJlbW92ZUNsYXNzKCdzaG93LWNoZWNrJyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSxcbiAgICAgICAgICB1bmhpZ2hsaWdodDogICAgZnVuY3Rpb24gKGVsZW1lbnQsIGVycm9yQ2xhc3MsIHZhbGlkQ2xhc3MpIHtcbiAgICAgICAgICAgIHZhciAkZWwgICAgID0gdmFsaWRhdG9yLmRlZmluZUVsZW1lbnQoJChlbGVtZW50KSksXG4gICAgICAgICAgICAgICAgJGVsV3JhcCA9ICRlbC5jbG9zZXN0KCcuZWwtZmllbGQnKTtcbiAgICAgICAgICAgIGlmICgkZWwpIHtcbiAgICAgICAgICAgICAgJGVsLnJlbW92ZUNsYXNzKGVycm9yQ2xhc3MpLmFkZENsYXNzKHZhbGlkQ2xhc3MpO1xuICAgICAgICAgICAgICBpZiAoJGVsV3JhcC5oYXNDbGFzcygnY2hlY2stdmFsaWQnKSkge1xuICAgICAgICAgICAgICAgICRlbFdyYXAuYWRkQ2xhc3MoJ3Nob3ctY2hlY2snKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAkZWwuY2xvc2VzdCgnZWwtZmllbGQnKS5hZGRDbGFzcygnc2hvdy1jaGVjaycpXG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSxcbiAgICAgICAgICBtZXNzYWdlczoge1xuICAgICAgICAgICAgJ3VzZXJfZW1haWwnOiB7XG4gICAgICAgICAgICAgIHJlcXVpcmVkOiAn0JLQstC10LTQuNGC0LUnLFxuICAgICAgICAgICAgICBlbWFpbDogJ9CS0LDQu9C40LQnXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgJ3VzZXJfbmFtZSc6IHtcbiAgICAgICAgICAgICAgcmVxdWlyZWQ6ICfQktCy0LXQtNC40YLQtScsXG4gICAgICAgICAgICAgIGxldHRlcnM6ICfQktCw0LvQuNC0J1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgICB9XG4gICAgfSk7XG4gIH0sXG4gIHNldEVycm9yOiAgICAgIGZ1bmN0aW9uICgkZWwsIG1lc3NhZ2UpIHtcbiAgICAkZWwgPSB0aGlzLmRlZmluZUVsZW1lbnQoJGVsKTtcbiAgICBpZiAoJGVsKSB0aGlzLmRvbVdvcmtlci5lcnJvcigkZWwsIG1lc3NhZ2UpO1xuICB9LFxuICBkZWZpbmVFbGVtZW50OiBmdW5jdGlvbiAoJGVsKSB7XG4gICAgcmV0dXJuICRlbDtcbiAgfSxcbiAgZG9tV29ya2VyOiAgICAge1xuICAgIGVycm9yOiBmdW5jdGlvbiAoJGVsLCBtZXNzYWdlKSB7XG4gICAgICBpZiAoJGVsLmF0dHIoJ3R5cGUnKSA9PSAnZmlsZScpICRlbC5wYXJlbnQoKS5hZGRDbGFzcygnZmlsZS1lcnJvcicpO1xuICAgICAgJGVsLmFkZENsYXNzKCdlcnJvcicpO1xuICAgICAgJGVsLmFmdGVyKG1lc3NhZ2UpO1xuICAgIH1cbiAgfVxufTtcblxuXG4vLyBydWxlIGZvciBmb3JtIG5hbWVzcGFjZVxuLy8tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxudmFyIHZhbGl0YXRvclJ1bGVzID0ge1xuICAnZm9ybV9zdWJzY3JpcHRpb24nOiB7XG4gICAgJ3VzZXJfZW1haWwnOiB7XG4gICAgICByZXF1aXJlZDogdHJ1ZSxcbiAgICAgIGVtYWlsOiB0cnVlXG4gICAgfVxuICB9LFxuXG4gICdmb3JtX29uZUNsaWNrJzoge1xuICAgICd1c2VyX25hbWUnOiB7XG4gICAgICByZXF1aXJlZDogdHJ1ZSxcbiAgICAgIGxldHRlcnM6IHRydWVcbiAgICB9LFxuICAgICd1c2VyX2VtYWlsJzoge1xuICAgICAgcmVxdWlyZWQ6IHRydWUsXG4gICAgICBlbWFpbDogdHJ1ZVxuICAgIH0sXG4gICAgJ3VzZXJfcGhvbmUnOiB7XG4gICAgICByZXF1aXJlZDogdHJ1ZSxcbiAgICAgIGRpZ2l0czogdHJ1ZVxuICAgIH1cbiAgfSxcblxuICAnZm9ybV9sb2dpbic6IHtcbiAgICAndXNlcl9sb2dpbic6IHtcbiAgICAgIHJlcXVpcmVkOiB0cnVlXG4gICAgfSxcbiAgICAndXNlcl9wYXNzd29yZCc6IHtcbiAgICAgIHJlcXVpcmVkOiB0cnVlLFxuICAgICAgbWlubGVuZ3RoOiA2XG4gICAgfVxuICB9LFxuXG4gICdmb3JtX3JlZ2lzdHJ5Jzoge1xuICAgICd1c2VyX2VtYWlsJzoge1xuICAgICAgcmVxdWlyZWQ6IHRydWUsXG4gICAgICBlbWFpbDogdHJ1ZVxuICAgIH0sXG4gICAgJ3VzZXJfcGFzc3dvcmQnOiB7XG4gICAgICByZXF1aXJlZDogdHJ1ZSxcbiAgICAgIG1pbmxlbmd0aDogNlxuICAgIH0sXG4gICAgJ3VzZXJfcGFzc3dvcmRfY29uZmlybSc6IHtcbiAgICAgIHJlcXVpcmVkOiB0cnVlLFxuICAgICAgbWlubGVuZ3RoOiA2LFxuICAgICAgZXF1YWxUbzogXCIjdXNlcl9wYXNzd29yZFwiXG4gICAgfVxuICB9XG59O1xuXG4vLyBjdXN0b20gcnVsZXNcbi8vLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiQudmFsaWRhdG9yLmFkZE1ldGhvZChcImVtYWlsXCIsIGZ1bmN0aW9uICh2YWx1ZSkge1xuICBpZiAodmFsdWUgPT0gJycpIHJldHVybiB0cnVlO1xuICB2YXIgcmVnZXhwID0gL1thLXowLTkhIyQlJicqKy89P15fYHt8fX4tXSsoPzpcXC5bYS16MC05ISMkJSYnKisvPT9eX2B7fH1+LV0rKSpAKD86W2EtejAtOV0oPzpbYS16MC05LV0qW2EtejAtOV0pP1xcLikrW2EtejAtOV0oPzpbYS16MC05LV0qW2EtejAtOV0pPy87XG4gIHJldHVybiByZWdleHAudGVzdCh2YWx1ZSk7XG59KTtcblxuJC52YWxpZGF0b3IuYWRkTWV0aG9kKFwibGV0dGVyc1wiLCBmdW5jdGlvbiAodmFsdWUsIGVsZW1lbnQpIHtcbiAgcmV0dXJuIHRoaXMub3B0aW9uYWwoZWxlbWVudCkgfHwgL15bXjEtOSFAI1xcJCVcXF4mXFwqXFwoXFwpXFxbXFxdOjssLj89K188PmB+XFxcXFxcL1wiXSskL2kudGVzdCh2YWx1ZSk7XG59KTtcblxuJC52YWxpZGF0b3IuYWRkTWV0aG9kKFwiZGlnaXRzXCIsIGZ1bmN0aW9uICh2YWx1ZSwgZWxlbWVudCkge1xuICByZXR1cm4gdGhpcy5vcHRpb25hbChlbGVtZW50KSB8fCAvXihcXCs/XFxkKyk/XFxzKihcXChcXGQrXFwpKT9bXFxzLV0qKFtcXGQtXSopJC9pLnRlc3QodmFsdWUpO1xufSk7XG5cbi8vICB2YWxpZGF0b3IgaW5pdFxuLy8tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxudmFsaWRhdG9yLmluaXQoKTtcbiJdfQ==
