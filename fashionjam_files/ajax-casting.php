<div class="modal">
	<div class="title">
		<h2>PAID Lingerie/Intimates Shoot for Fashion Book</h2>
		<a class="icon-close" href="javascript:jQuery.fancybox.close();"><svg><use xlink:href="#icon-close"></use></svg></a>
	</div>

	<div class="content-modal">
		<div class="btn-wrap">
			<a href="#" class="el-btn bg-none">APPLY</a>
		</div>
		<h2>PAID Lingerie/Intimates Shoot for Fashion Book</h2>


		<ul class="user-info">
			<li><svg><use xlink:href="#icon-sye"></use></svg>Looking for: <span>Make up Artists.</span></li>
			<li><svg><use xlink:href="#icon-location"></use></svg>Location: <span>Turkey, Istanbul.</span></li>
			<li><svg><use xlink:href="#icon-cub"></use></svg>Rate: <span>$100.</span></li>
		</ul>

		<ul class="list-photos">
			<li><img src="images/portfolio/img-1.jpg" alt=""></li>
			<li><img src="images/portfolio/img-2.jpg" alt=""></li>
			<li><img src="images/portfolio/img-3.jpg" alt=""></li>
		</ul>

		<h3>Details:</h3>
		<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of usin</p>
		<p>Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.
		Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum'</p>
		<p>will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>
		<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some </p>

	</div>

	<div class="content-footer">
		<div class="user-wrap">
			<div class="titl-motal">
				PHOTO BY
			</div>
			<div class="item-owner">
				<div class="user-pick">
					<img src="images/img-13.jpg" alt="">
				</div>
				<div class="user">
					<a href="#" class="user-name">Emily Larson</a>
					<div class="user-prof">Model</div>
					<ul class="user-info">
						<li><svg><use xlink:href="#icon-location"></use></svg><span>Kyiv, Ukraine</span></li>
					</ul>
				</div>
			</div>
			<ul class="infos">
				<li><svg><use xlink:href="#icon-sye"></use></svg><strong>View: </strong><span>158</span></li>
				<li><svg><use xlink:href="#icon-comment"></use></svg><strong>Comments: </strong><span>9</span></li>
			</ul>
			<ul class="tags">
				<li><a href="#">#model</a></li>
				<li><a href="#">#fashion</a></li>
				<li><a href="#">#boy</a></li>
				<li><a href="#">#girl</a></li>
			</ul>
			<ul class="share">
				<li><a href="#"><svg><use xlink:href="#icon-soc-1"></use></svg></a></li>
				<li><a href="#"><svg><use xlink:href="#icon-soc-2"></use></svg></a></li>
				<li><a href="#"><svg><use xlink:href="#icon-soc-4"></use></svg></a></li>
			</ul>
		</div>
		<div class="comment">
			<div class="titl-motal">
				Comments
			</div>
			<ul class="list-comment">
				<li>
					<div class="user-pick">
						<img src="images/img-13.jpg" alt="">
					</div>
					<div class="wrap-info">
						<a href="#" class="user-name">
							Anna Lambrechts
							<span class="time">
								Yesterday at 17:09
							</span>
						</a>
						<div class="user-coment">
							Amazing photo
						</div>
					</div>
				</li>
				<li>
					<div class="user-pick">
						<img src="images/img-13.jpg" alt="">
					</div>
					<div class="wrap-info">
						<a href="#" class="user-name">
							Anna Lambrechts
							<span class="time">
								Yesterday at 17:09
							</span>
						</a>
						<div class="user-coment">
							Amazing photo
						</div>
					</div>
				</li>
				<li>
					<div class="user-pick">
						<img src="images/img-13.jpg" alt="">
					</div>
					<div class="wrap-info">
						<form class="#" action="#" method="post">
							<input type="text" name="mess" value="" placeholder="Comment...">
						</form>
					</div>
				</li>
			</ul>
		</div>

	</div>
</div>
