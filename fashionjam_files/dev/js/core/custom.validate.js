// custom jQuery validation
//-----------------------------------------------------------------------------------
var validator = {
  init:          function () {
    $('form').each(function () {
      var name = $(this).attr('name');
      if (valitatorRules.hasOwnProperty(name)) {
        var rules = valitatorRules[name];
        $(this).validate({
          rules:          rules,
          errorElement:   'b',
          errorClass:     'error',
          focusInvalid:   true,
          focusCleanup:   false,
          errorPlacement: function (error, element) {
            validator.setError($(element), error);
          },
          highlight:      function (element, errorClass, validClass) {
            var $el     = validator.defineElement($(element)),
                $elWrap = $el.closest('.el-field');
            if ($el) {
              $el.removeClass(validClass).addClass(errorClass);
              $elWrap.removeClass('show-check');
            }
          },
          unhighlight:    function (element, errorClass, validClass) {
            var $el     = validator.defineElement($(element)),
                $elWrap = $el.closest('.el-field');
            if ($el) {
              $el.removeClass(errorClass).addClass(validClass);
              if ($elWrap.hasClass('check-valid')) {
                $elWrap.addClass('show-check');
              }
              $el.closest('el-field').addClass('show-check')
            }
          },
          messages: {
            'user_email': {
              required: 'Введите',
              email: 'Валид'
            },
            'user_name': {
              required: 'Введите',
              letters: 'Валид'
            }
          }
        });
      }
    });
  },
  setError:      function ($el, message) {
    $el = this.defineElement($el);
    if ($el) this.domWorker.error($el, message);
  },
  defineElement: function ($el) {
    return $el;
  },
  domWorker:     {
    error: function ($el, message) {
      if ($el.attr('type') == 'file') $el.parent().addClass('file-error');
      $el.addClass('error');
      $el.after(message);
    }
  }
};


// rule for form namespace
//-----------------------------------------------------------------------------------
var valitatorRules = {
  'form_subscription': {
    'user_email': {
      required: true,
      email: true
    }
  },

  'form_oneClick': {
    'user_name': {
      required: true,
      letters: true
    },
    'user_email': {
      required: true,
      email: true
    },
    'user_phone': {
      required: true,
      digits: true
    }
  },

  'form_login': {
    'user_login': {
      required: true
    },
    'user_password': {
      required: true,
      minlength: 6
    }
  },

  'form_registry': {
    'user_email': {
      required: true,
      email: true
    },
    'user_password': {
      required: true,
      minlength: 6
    },
    'user_password_confirm': {
      required: true,
      minlength: 6,
      equalTo: "#user_password"
    }
  }
};

// custom rules
//-----------------------------------------------------------------------------------
$.validator.addMethod("email", function (value) {
  if (value == '') return true;
  var regexp = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
  return regexp.test(value);
});

$.validator.addMethod("letters", function (value, element) {
  return this.optional(element) || /^[^1-9!@#\$%\^&\*\(\)\[\]:;,.?=+_<>`~\\\/"]+$/i.test(value);
});

$.validator.addMethod("digits", function (value, element) {
  return this.optional(element) || /^(\+?\d+)?\s*(\(\d+\))?[\s-]*([\d-]*)$/i.test(value);
});

//  validator init
//-----------------------------------------------------------------------------------
validator.init();
