
<div class="modal">
	<div class="title">
		<h2>Spring Fashion Mahazine 2016</h2>
		<a class="icon-close" href="javascript:jQuery.fancybox.close();"><svg><use xlink:href="#icon-close"></use></svg></a>
	</div>

	<div class="content-modal">
		<img src="<?echo 'images/portfolio/'.$_POST[id].'.jpg';?>" alt="img">
	</div>

	<div class="content-footer">
		<div class="user-wrap">
			<div class="titl-motal">
				PHOTO BY
			</div>
			<div class="item-owner">
				<div class="user-pick">
					<img src="images/img-13.jpg" alt="">
				</div>
				<div class="user">
					<a href="#" class="user-name">Emily Larson</a>
					<div class="user-prof">Model</div>
					<ul class="user-info">
						<li><svg><use xlink:href="#icon-location"></use></svg><span>Kyiv, Ukraine</span></li>
					</ul>
				</div>
			</div>
			<ul class="infos">
				<li><svg><use xlink:href="#icon-sye"></use></svg><strong>View: </strong><span>158</span></li>
				<li><svg><use xlink:href="#icon-comment"></use></svg><strong>Comments: </strong><span>9</span></li>
			</ul>
			<ul class="tags">
				<li><a href="#">#model</a></li>
				<li><a href="#">#fashion</a></li>
				<li><a href="#">#boy</a></li>
				<li><a href="#">#girl</a></li>
			</ul>
			<ul class="share">
				<li><a href="#"><svg><use xlink:href="#icon-soc-1"></use></svg></a></li>
				<li><a href="#"><svg><use xlink:href="#icon-soc-2"></use></svg></a></li>
				<li><a href="#"><svg><use xlink:href="#icon-soc-4"></use></svg></a></li>
			</ul>
		</div>
		<div class="comment">
			<div class="titl-motal">
				Comments
			</div>
			<ul class="list-comment">
				<li>
					<div class="user-pick">
						<img src="images/img-13.jpg" alt="">
					</div>
					<div class="wrap-info">
						<a href="#" class="user-name">
							Anna Lambrechts
							<span class="time">
								Yesterday at 17:09
							</span>
						</a>
						<div class="user-coment">
							Amazing photo
						</div>
					</div>
				</li>
				<li>
					<div class="user-pick">
						<img src="images/img-13.jpg" alt="">
					</div>
					<div class="wrap-info">
						<a href="#" class="user-name">
							Anna Lambrechts
							<span class="time">
								Yesterday at 17:09
							</span>
						</a>
						<div class="user-coment">
							Amazing photo
						</div>
					</div>
				</li>
				<li>
					<div class="user-pick">
						<img src="images/img-13.jpg" alt="">
					</div>
					<div class="wrap-info">
						<form class="#" action="#" method="post">
							<input type="text" name="mess" value="" placeholder="Comment...">
						</form>
					</div>
				</li>
			</ul>
		</div>

	</div>
</div>
