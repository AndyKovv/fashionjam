import logging
from aiohttp import web
from fashionjam.app import init_app


def run():
    logging.basicConfig(level=logging.DEBUG)
    app = init_app()
    web.run_app(app, host=app['config']['host'], port=app['config']['port'])

if __name__ == '__main__':
    run()

# API_Layer -> ADAPTER/FACADE
# ADAPTER -> Instances Layers
# InstanceLayer -> DB_layer
# DB_layer -> model


# ADAPTER / FACADE
#   -> DTO Data transfer object
#   -> Instance Layer
#   -> Binding logic

# Instance
#   -> DTO Data Trasfer Object
#   -> Business logic
#   -> Event signals

# DB_layer
#   -> DTO Data Trasfer Object
#   -> DAO
