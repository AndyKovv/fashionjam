# 2017 Andrii Kovalov andy.kovv@gmail.com


class UserCacheKey:
    """
        Class provide user cache keys management
        :user_id - database user instance
    """

    def __init__(self, user_id):
        self._user_id = user_id

    def __call__(self, user_id):
        """ Validate user_id """
        if not isinstance(user_id, int):
            raise ValueError('user_id must be integer')

    def get_user_cache_key(self):
        """ Method should return user formatted instance """
        return f"user:{self._user_id}:instance"
