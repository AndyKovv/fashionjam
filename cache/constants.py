# 2017 Andrii Kovalov andy.kovv@gmail.com

from enum import Enum


class UserCacheErrors(Enum):
    """ Class implements user cache errors message """
    ERROR_USER_CACHE = 'User data invalid'
