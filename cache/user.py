# 2017 Andrii Kovalov andy.kovv@gmail.com

from functools import partial
from core.exceptions.http_exceptions import HttpErrorException
from cache.keys.user_keys import UserCacheKey as UCK
from cache.constants import UserCacheErrors as UCE
from core.schemas.user_schemas.user_schema import UserCacheInstance

class UserCacheManager:
    """
        Class implement user management in cahce
        :conn - redis connection instance
        :user_id - id user for fetch from cache
        :key - default redis key
    """

    def __init__(self, app, user_id, key=None):
        self._conn = app['redis']
        self._loop = app.loop
        self._user_id = user_id
        self._key = key
        self._user_data = dict()

    async def get(self):
        """
            Method should return user instance from redis
            if instance not found return exception
        """
        user_data = await self._conn.hgetall(self.key, encoding='utf-8')
        return user_data

    def get_patial_fields(self):
        """
            Method should return partial field
            Default partial fields get from UserCacheInstance
        """
        return self.get_serializer().cache_user_partial()

    async def add_to_cache(self):
        """ Procedure should add user instance to cache """
        user_data = await self.__serialize_user_data()
        # TODO Add remove key interval
        self._conn.hmset_dict(self.key, user_data)
        return True

    def get_serializer(self):
        """
            Method should return serializer
            when method owerride just return link for class
        """
        return UserCacheInstance

    async def __serialize_user_data(self):
        """ Method should serialize user data """
        # Add to executer schema
        schema = await self._loop.run_in_executor(None, self.get_serializer())

        # Serialize schema in executor
        serialized_data = await self._loop.run_in_executor(
            None,
            partial(
                schema.load,
                self._user_data,
                partial=self.get_patial_fields()
            )
        )
        # Validate shema and raise error
        errors = serialized_data.errors
        if errors:
            print(errors)
            raise HttpErrorException(UCE.ERROR_USER_CACHE.name, 400)

        # Return default data
        return serialized_data.data

    @property
    def key(self):
        """ Method should return formatted user key """
        return UCK(self._user_id).get_user_cache_key()

    @property
    def user_data(self):
        return self._user_data

    @user_data.setter
    def user_data(self, value):
        self._user_data = value
        return True
