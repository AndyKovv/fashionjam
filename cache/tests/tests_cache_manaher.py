# 2017 Andrii Kovalov andy.kovv@gmail.com

from core.helpers.test_case import AioHttpGetApp, unittest_run_loop
from ..user import UserCacheManager


class UserCacheManagerTests(AioHttpGetApp):

    def setUp(self):
        super().setUp()

    # async def setUpAsync(self):
    #     s

    # async def tearDownAsync(self):

    @unittest_run_loop
    async def test_should_add_user_to_cache_and_get_from_cahe(self):
        manager = UserCacheManager(self.app, user_id=1)
        data = {
            'user_id': '1',
            'email': 'some@mail.com',
            'first_name': 'Andy',
            'password_hash': 'some_strong_pass',
            'last_seen': '1234433',
        }
        manager.user_data = data
        self.assertEqual(manager.key, 'user:1:instance')
        created = await manager.add_to_cache()
        self.assertTrue(created)
        user = await manager.get()
        self.assertEquals(user, data)

    # async def test_should_add_and_update_new_key_to_database_without_transaction(self):
    #     requests = [self.client.get('/') for _ in range(1, 1000)]
    #     tasks = await asyncio.gather(*requests)
    #     print(tasks)
    #     print(self.server.port)
    #     # new_connection = self.redis_conn.connection.acquire
    #     # sec_connection = self.redis_conn.connection.get_connection('get')
    #     # connection = self.redis_conn.connection
    #     # set_state = await self.redis_conn.set('new_key', 'value')
    #     # print(connection.freesize)
    #     # print(new_connection)
    #     self.assertEqual(1, 2)
