# 2018 Andrii Kovalov andy.kovv@gmail.com
'''
    Social Auth Provider
    Implement FaceBook and Instagram Auth
'''
import re
import sqlalchemy as sa
from sqlalchemy.sql import or_
from functools import partial
from core.db.models.social_auth import social_providers
from core.exceptions.http_exceptions import HttpErrorException
from core.schemas.social_auth_schema import SocialProviderSchema
from core.contrib.utils import Utils


class SocialProviderManager:
    """
        Class impement management for social auth
        :app - app instance
        :name -  provider name (must be unique)
        :provider_url - raw provider url
        :app_token - token for auth on provider
    """

    to_dict = Utils.to_dict
    pattern = Utils.url_pattern

    exception_class = HttpErrorException
    db_model = social_providers
    serializer = SocialProviderSchema

    def __init__(self, app, **kwargs):
        self.__db = app['db']
        self.__loop = app.loop
        self.__raw_fields = kwargs

        self.__provider_id = kwargs.get('provider_id')
        self.__name = kwargs.get('name')
        self.__provider_url = kwargs.get('provider_url')
        self.__app_token = kwargs.get('app_token')
        self.__app_secret = kwargs.get('app_secret')
        self.__api_version = kwargs.get('api_version')
        self.__is_active = kwargs.get('is_active', False)

        self.__provider_instance = dict()
        self.__provider_serialized_data = dict()
        self.__errors = dict()

    @property
    def exception(self):
        """
            Property should return exception class
            default return HttpErrorException
        """
        if not self.exception_class:
            return HttpErrorException
        return self.exception_class

    @property
    def errors(self):
        return self.__errors

    @property
    def provider(self):
        return self.__provider_instance

    @property
    def data(self):
        # Serialize providers from database
        return self.__provider_serialized_data

    @errors.setter
    def errors(self, value):
        """ Setter should update errors """
        if not isinstance(value, dict):
            value_type = type(value)
            raise ValueError(f'Value must be dict, but receive {value_type}')
        self.__errors.update(value)

    def get_model_fields(self):
        """ Method should return model fields """
        return [
            self.db_model.c.provider_id, self.db_model.c.provider_url,
            self.db_model.c.redirect_url, self.db_model.c.name,
            self.db_model.c.app_token, self.db_model.c.app_secret,
            self.db_model.c.api_version, self.db_model.c.is_active
        ]

    async def create_provider(self):
        """ Procedure should implement provider creation """
        await self.__validate_on_save()
        # Format query
        add_provider_query = (
            self.db_model.insert()
            .returning(*self.db_model.c)
            .values(**self.__raw_fields)
        )
        async with self.__db.acquire() as conn:
            created_provider = await conn.execute(add_provider_query)
            self.__provider_instance = await created_provider.fetchone()
        return True

    async def get_active_providers(self):
        """
            Procedure should fetch active providers from database
        """
        active_providers_query = (
            sa.select(
                self.get_model_fields()
            ).where(self.db_model.c.is_active == True)  # noqa
        )
        async with self.__db.acquire() as conn:
            cursor = await conn.execute(active_providers_query)
            providers_list = await cursor.fetchall()

        # Set provider instance for serialization
        self.__provider_instance = self.to_dict(providers_list)
        await self.__serialize_provider()
        return True

    async def get_provider(self):
        """ Procedure should get provider from database """
        self.__check_provider_id_exists()
        # Check if provider exists in database
        provider_query = (
            sa.select(
                self.get_model_fields()
            ).where(self.db_model.c.provider_id == self.__provider_id)
        )
        async with self.__db.acquire() as conn:
            cursor = await conn.execute(provider_query)
            provider = await cursor.fetchone()

        # Check if provider exists
        if not provider:
            # Return empty dict
            return {}

        # Serialize data
        self.__provider_instance = self.to_dict(provider)
        await self.__serialize_provider(many=False)
        return True

    async def disable_provider(self):
        """
            Procedure should disable provider,
            raise exception if provider id not defined
        """
        # Validate income data before toggle it
        await self.__validate_defined_provider()
        await self.__provider_toggle_procedure(False)
        return True

    async def enable_provider(self):
        """ Procedure should enable provider """

        # Validate income data before toggle it
        await self.__validate_defined_provider()
        await self.__provider_toggle_procedure(True)
        return True

    def __check_provider_id_exists(self):
        """ Procedure should check if provider_id  exists """
        if not isinstance(self.__provider_id, int):
            self.errors = {'provider_id': 'Must be integer'}
            raise HttpErrorException(self.errors, 400)

        if not self.__provider_id:
            self.errors = {'provider_id': 'You must specify provider id'}
            raise HttpErrorException(self.errors, 400)
        return True

    async def __validate_defined_provider(self):
        """ Procedure should validate income id for None and provider in database """
        self.__check_provider_id_exists()

        # Get provider by id and check if it exists
        await self.get_provider()
        if not self.data:
            self.errors = {'provider_error': f'Provider with id:{self.__provider_id} not found'}
            raise HttpErrorException(self.errors, 400)
        return True

    async def __provider_toggle_procedure(self, provider_status):
        """
            Procedure should enable or disable provider
            :provider_status must be boolean value
        """
        if not isinstance(provider_status, bool):
            raise ValueError(f'Provider status must be boolean not {type(provider_status)}')

        provider_update_query = (
            self.db_model
            .update()
            .values(is_active=provider_status)
            .where(self.db_model.c.provider_id == self.__provider_id)
        )
        async with self.__db.acquire() as conn:
            await conn.execute(provider_update_query)

        self.__provider_instance.update({'is_active': provider_status})
        return True

    async def __serialize_provider(self, with_validation=False, many=True):
        """ Procedure should serialize provider data """
        serializer = await self.__loop.run_in_executor(
            None, partial(self.serializer, many=many)
        )
        social_providers = await self.__loop.run_in_executor(
            None, partial(serializer.dump, self.__provider_instance)
        )
        # Check if validate income data
        if with_validation:
            self.errors = social_providers.errors
            if self.errors:
                raise self.exception(self.errors, 400)
        self.__provider_serialized_data = social_providers.data
        return True

    async def __validate_on_save(self):
        """
            Procedure should validate income data and check already
            record exist in database
        """
        # Check for url and validate it
        if not re.match(self.pattern(), self.__provider_url):
            self.errors = {
                'provider_url': ['Url invalid, please specify url by shema http/s://some-url.com/']
            }

        if not self.__name:
            self.errors = {'name': ['You must specify name provider']}

        if not self.__app_token:
            self.errors = {'app_token': ['App token not defined']}

        # Check if errors not empty
        if self.errors:
            # Raise exeption
            raise self.exception(self.errors, 400)

        # Check if record already exists
        fetch_provider_query = (
            sa.select(
                [
                    self.db_model.c.provider_id, self.db_model.c.name,
                    self.db_model.c.app_token
                ]
            )
            .where(
                or_(
                    self.db_model.c.name == self.__name,
                    self.db_model.c.app_token == self.__app_token
                )
            )
        )
        async with self.__db.acquire() as conn:
            cursor = await conn.execute(fetch_provider_query)
            provider = await cursor.fetchone()
        # If provider already exists add error and raise exception
        if provider:
            self.errors = {
                'provider_error': [
                    f"Provider with name: {provider['name']} or app_token: {provider['app_token']}"
                ]
            }
            raise self.exception(self.errors, 400)
        return True
