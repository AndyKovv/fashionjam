import ujson
from functools import partial
from aiojobs.aiohttp import spawn
from core.exceptions.http_exceptions import HttpErrorException
from core.schemas.user_schemas.user_schema import UserSchema # TODO Need rewright

from auth_service.password_manager import PasswordHash
from auth_service.jwt_manager import JWTManager
from auth_service.constants import UserResponse as ur


class AuthUser:
    """
        Class for implement auth user
        :app - main app
        :email - user email
        :password - user raw password
    """
    def __init__(self, app, email, raw_password, model):
        self._db = app['db']
        self._config = app['config']
        self._db_model = model
        self._email = email
        self._password = raw_password
        self._validate()
        self._user_payload = dict()

    async def login(self):
        """ Method should login user otherwise raise error """
        user = None
        async with self._db.acquire() as conn:
            cursor = await conn.execute(
                self._db_model.select()
                .where(self._db_model.c.email == self._email)
            )
            user = await cursor.fetchone()
        if not user:
            raise ValueError(ur.ERROR_AUTH.name)
        password_manager = PasswordHash(user['password'])
        if not password_manager.is_equal(self._password):
            raise HttpErrorException(ur.ERROR_AUTH.name, 401)
        jwt_payload = JWTManager(self._config, user['user_id']).create_token()
        self._user_payload = {
            'user_id': user['user_id'],
            'token': jwt_payload.token
        }
        return True

    async def response(self):
        return ujson.dumps(self._user_payload)

    def _validate(self):
        """ Procedure should validate input data for prevent error """
        if not self._email or not self._password:
            # TODO ADD CUSTOM EXCEPTION CLASS
            raise HttpErrorException(ur.ERROR_LOGIN_FIELDS.name, 401)
        return True


class RegisterUser:
    """
        Class implements user register
        :db - database instance
        :fields - request post data
    """
    schema = UserSchema
    jwt_manager = JWTManager
    partial_fields = (
        'id', 'avatar', 'is_active',
        'is_staff', 'is_superuser', 'date_of_birth'
    )

    def __init__(self, request, fields, user_model):
        self.validated_fields = None
        self._db = request.app['db']
        self._config = request.app['config']
        self._request = request
        self._loop = request.app.loop
        self._fields = fields
        self._model = user_model
        self._created_user = None

    async def persist(self):
        """ procedure should persist user to database """
        await self.__check_user_exists()

        # TODO IMPLEMENT TO CACHE
        # await spawn(self._request, self.save_db())
        await self.save_db()
        return True

    async def save_db(self):
        """ Procedure save user insance to database """
        async with self._db.acquire() as conn:
            created_user = await conn.execute(
                self._model.insert()
                .returning(*self._model.c)
                .values(**self.validated_fields)
            )
            self._created_user = await created_user.fetchone()
            return True

    async def response(self):
        """ Method should return response """
        if not self._created_user:
            raise HttpErrorException(ur.DB_ERROR, 400)

        user_id = self._created_user['user_id']
        jwt_token = JWTManager(self._config, user_id).create_token()
        payload = {
            'user_id': user_id,
            'token': jwt_token.token
        }
        return ujson.dumps(payload)

    async def __validate(self):
        """ Procedure should validate fields """
        schema = await self._loop.run_in_executor(None, self.schema)
        user_schema = await self._loop.run_in_executor(
            None,
            partial(schema.load, self._fields, partial=self.partial_fields)
        )
        errors = user_schema.errors
        if errors:
            raise HttpErrorException(errors, 400)
        user_data = user_schema.data

        # Hash user password
        hashed_password = PasswordHash.create_hash(user_data['password'])
        user_data.update({'password': hashed_password})
        self.validated_fields = user_data
        return True

    async def __check_user_exists(self):
        """ Procedure should check if email exist """
        await self.__validate()
        email = self.validated_fields['email']
        async with self._db.acquire() as conn:
            user_exist = await conn.scalar(
                self._model.select()
                .where(self._model.c.email == email)
            )
            if user_exist:
                raise HttpErrorException(ur.ERROR_USER_EXISTS.name, 400)
            return False
