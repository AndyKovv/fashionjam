
from auth_service.auth_api import RegisterApi, LoginApi, FacebookRegisterAPI

routes = [
    ('POST', '/api-auth/v1/register/', RegisterApi, 'api-register'),
    ('POST', '/api-auth/v1/login/', LoginApi, 'api-login'),
    ('GET', '/api-auth/v1/facebook-register/', FacebookRegisterAPI, 'facebook-register-api')
]
