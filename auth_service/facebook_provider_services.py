# 2018 Andrii Kovalov andy.kovv@gmail.com

from aiohttp import ClientSession
from core.exceptions.http_exceptions import HttpErrorException


class Facebook:
    """
        Class implement provider for facebook api
    """
    exception = HttpErrorException
    default_user_profile_fields = (
        'id', 'first_name', 'birthday', 'email', 'gender'
    )

    def __init__(self, app, **params):
        self.__app = app
        self.__code = params.get('code')
        self.__access_token = params.get('user_access_token')

        self.__sizes = params.get('sizes')
        self.__provider = params.get('provider', {})

        self.__token_response = dict()
        self.__token_validation_data = dict()
        self.__user_profile_data = dict()
        self.__user_photos = list()

        self.__facebook_domain = "https://graph.facebook.com/"
        self.__token_is_valid = False

    @property
    def api_version(self):
        version = self.__provider.get('api_version', None)
        if not version:
            version = 2.1
        return version

    @property
    def app_token(self):
        return self.__provider.get('app_token')

    @property
    def app_secret(self):
        return self.__provider.get('app_secret')

    @property
    def token_is_valid(self):
        return self.__token_is_valid

    @property
    def user_profile_data(self):
        return self.__user_profile_data

    @property
    def token_validation_data(self):
        return self.__token_validation_data

    @property
    def redirect_url(self):
        return self.__provider.get('redirect_url')

    @property
    def token_response(self):
        return self.__token_response

    @property
    def user_photos(self):
        return self.__user_photos

    def get_token_url(self):
        """ Method should format facebook token validation url """
        required_params = (self.app_token, self.redirect_url, self.__code)
        self.__validate_required_params(*required_params)
        return (
            f"{self.__facebook_domain}{self.api_version}/oauth/access_token?"
            f"client_id={self.app_token}&redirect_uri={self.redirect_url}"
            f"&client_secret={self.__code}"
        )

    def get_validation_token_url(self):
        """ Method should return token validation url """
        required_params = (self.__access_token, self.app_token, self.app_secret)
        self.__validate_required_params(*required_params)
        return (
            f"{self.__facebook_domain}debug_token?"
            f"input_token={self.__access_token}"
            f"&access_token={self.app_token}|{self.app_secret}"
        )

    def get_user_profile_url(self):
        """ Method should return url for fetching user profile data """
        required_params = (self.default_user_profile_fields, self.__access_token)
        self.__validate_required_params(*required_params)
        return (
            f"{self.__facebook_domain}me?fields="
            f"{','.join(self.default_user_profile_fields)}"
            f"&access_token={self.__access_token}"
        )

    def get_user_photo_url(self, photo_size):
        """
            Method should return url for fetching user photo
            :photo_size - tuple with (height:int, width:int)
        """
        required_params = (self.__access_token,)
        self.__validate_required_params(*required_params)
        return (
            f"https://graph.facebook.com/me/picture?"
            f"height={photo_size[0]}&width={photo_size[1]}&access_token="
            f"{self.__access_token}&redirect=0"
        )

    async def get_user_photos(self):
        """ Procedure should fetch user photos from facebook api """
        self.__validate_photos_sizes()
        user_photos = list()
        for photo_size in self.__sizes:
            photo_url = self.get_user_photo_url(photo_size)
            photo_data = await self.__fetch_from_facebook_api(photo_url)
            user_photos.append(photo_data)
        self.__user_photos = user_photos
        return True

    async def get_user_profile_data(self):
        """ Frocedure should fetch user profile from facebook api """
        response = await self.__fetch_from_facebook_api(self.get_user_profile_url())
        self.__user_profile_data.update(response)
        return True

    async def get_user_token(self):
        """ Procedure should fetch user token from facebook server """
        # response return as dict instance
        response = await self.__fetch_from_facebook_api(self.get_token_url())
        self.__access_token = response.get('access_token')

        self.__token_response.update(response)
        self.__token_valid()
        return True

    async def fetch_user_data(self):
        """ Proccedure should fetch user instance from api """
        await self.get_user_token()
        await self.validate_token()
        await self.get_user_profile_data()
        await self.get_user_photos()
        return True

    async def validate_token(self):
        """ Procedure should validate user token on facebook api """
        response = await self.__fetch_from_facebook_api(self.get_validation_token_url())
        self.__token_validation_data.update(response)

        # Check if data exists in response then check validation status
        data = response.get('data')
        if not data:
            # If no data found raise exception
            raise self.exception(
                f'Facebook api returned uknown result on token validation',
                400
            )

        if not data.get('is_valid'):
            self.__token_is_valid()
            return False

        self.__token_valid()
        return True

    async def __fetch_from_facebook_api(self, url):
        """
            Method should return response instance and
            check response for error key
        """
        async with ClientSession() as session:
            async with session.get(url) as resp:
                json_response = await resp.json()
        self.__error_handle(json_response)
        return json_response

    def __validate_photos_sizes(self):
        """ Procedure should validate income photo sizes """
        valid_type = self.__sizes and isinstance(self.__sizes, tuple)
        if not valid_type:
            raise ValueError('Photos must be not empty and type tuple')

        for sizes in self.__sizes:
            is_tuple = isinstance(sizes, tuple)
            if not is_tuple:
                raise ValueError(
                    f'Sizes must be tuple with (height, width), get {sizes}'
                )
            valid_size = len(sizes) == 2
            if not valid_size:
                raise ValueError(
                    f'Tuple with sizes must be 2 element not {len(sizes)}'
                )
            elements_is_int = (
                isinstance(sizes[0], int) and isinstance(sizes[1], int)
            )
            if not elements_is_int:
                raise ValueError(
                    f"All elements of tupele must be int "
                )
        return True

    def __validate_required_params(self, *params):
        """
            Procedure should validate required params
            :params - tuple of params
        """
        for param in params:
            if not param:
                raise self.exception(f'Param not defined {param}', 400)
        return True

    def __error_handle(self, response):
        """ Procedure should check response for error message """
        error_payload = response.get('error')
        if not error_payload:
            return True
        raise self.exception(error_payload, 400)

    def __token_valid(self):
        """ Procedure should change toke valid status """
        self.__token_is_valid = True
        return True

    def __token_invalid(self):
        """ Procedure should change token valid status to invalid """
        self.__token_is_valid = False
        return True
