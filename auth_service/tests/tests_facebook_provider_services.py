# 2018 Andrii Kovalov andy.kovv@gmail.com
"""
    Tests should describe facebook authentication
"""

from aioresponses import aioresponses
from core.helpers.test_case import AioHttpGetApp, unittest_run_loop
from core.exceptions.http_exceptions import HttpErrorException
from auth_service.facebook_provider_services import Facebook
from auth_service.tests.fixtures import (
    drop_user_table, FacebookProviderFixtures, drop_social_provider_table
)


class FacebookProviderTests(AioHttpGetApp):
    """ Service for testing facebook OAuth """

    async def setUpAsync(self):
        # await drop_social_provider_table(self.app)
        self.facebook_factory = FacebookProviderFixtures()
        self.fb_provider = await self.facebook_factory.add_provider(self.app)
        self.redirect_url = self.fb_provider['redirect_url']
        self.provider = self.fb_provider['provider_url']
        self.client_id = self.fb_provider['app_token']
        self.app_secret = self.fb_provider['app_secret']
        self.api_version = self.fb_provider['api_version']

        self.code = 'some_acess_code'
        self.token_response = {
            'access_token': 'SomeFacebookUserToken',
            'token_type': 'bearer',
            'expires_in': 5179653,
        }
        self.validation_response = {
            "data": {
               "app_id": "111905079617932",
               "type": "USER",
               "application": "fashionjam_test",
               "expires_at": 1521058532,
               "is_valid": True,
               "issued_at": 1515874532,
               "scopes": [
                  "public_profile"
               ],
               "user_id": "363457047398968"
            }
        }
        self.facebook_error_response = {
            "error": {
               "message": "This authorization code has expired.",
               "type": "OAuthException",
               "code": 100,
               "fbtrace_id": "Gz+HNgVJ2tO"
            }
        }

        self.user_access_token = self.token_response['access_token']
        self.get_token_url = (
            f"https://graph.facebook.com/{self.api_version}"
            f"/oauth/access_token?client_id={self.client_id}"
            f"&redirect_uri={self.redirect_url}&client_secret={self.code}"
        )
        self.validate_token_url = (
            f"https://graph.facebook.com/debug_token?"
            f"input_token={self.token_response['access_token']}"
            f"&access_token={self.client_id}|{self.app_secret}"
        )
        self.user_profile_url = (
            f"https://graph.facebook.com/me?fields="
            f"id,first_name,birthday,email,gender&access_token="
            f"{self.token_response['access_token']}"
        )
        self.fb_user_profile_data = {
            'first_name': 'Andy',
            'gender': 'male',
            'email': 'andy.kovv@gmail.com',
            'id': '363457047398968',
            'birthday': '07/22/1991',
        }

        self.user_photo_url = (
            f"https://graph.facebook.com/me/picture?"
            f"height=200&width=200&access_token="
            f"{self.token_response['access_token']}&redirect=0"
        )

        self.photo_response = {
            "data": {
                "height": 200,
                "is_silhouette": False,
                "url": "https://scontent.xx.fbcdn.net/v/t1.0-1/p200x200/ \
                14955882_209667042777970_624717026356775021_n.jpg?oh= \
                ff3dc26c431e548c6555b1037baa00ab&oe=5AFBDC5A",
                "width": 200
            }
        }

    async def tearDownAsync(self):
        await drop_user_table(self.app)
        await drop_social_provider_table(self.app)

    @unittest_run_loop
    @aioresponses()
    async def test_should_get_and_validate_user_token(self, m):
        """
            Test should send request to facebook for
            validate user token
        """
        sizes = ((200, 200),)
        fb_manager = Facebook(
            self.app,
            code=self.code,
            sizes=sizes,
            provider=self.fb_provider,
        )

        m.get(self.get_token_url, status=200, payload=self.token_response)
        await fb_manager.get_user_token()
        self.assertEqual(
            fb_manager.token_response['access_token'],
            self.token_response['access_token']
        )
        self.assertEqual(
            fb_manager.token_response['token_type'],
            self.token_response['token_type']
        )
        self.assertEqual(
            fb_manager.token_response['expires_in'],
            self.token_response['expires_in']
        )
        self.assertTrue(fb_manager.token_is_valid)

        m.get(
            self.validate_token_url, status=200,
            payload=self.validation_response
        )
        await fb_manager.validate_token()
        self.assertTrue(fb_manager.token_is_valid)
        self.assertEqual(
            fb_manager.token_validation_data['data']['expires_at'],
            self.validation_response['data']['expires_at']
        )
        self.assertEqual(
            fb_manager.token_validation_data['data']['user_id'],
            self.validation_response['data']['user_id']
        )

        # Add parser for user_profile data
        m.get(self.user_profile_url, status=200, payload=self.fb_user_profile_data)
        await fb_manager.get_user_profile_data()
        self.assertEquals(fb_manager.user_profile_data, self.fb_user_profile_data)

        # Get user_photo or photos
        m.get(self.user_photo_url, status=200, payload=self.photo_response)
        await fb_manager.get_user_photos()

        # Get first tuple from list
        self.assertEqual(
            fb_manager.user_photos[0]['data']['url'], self.photo_response['data']['url']
        )
        # {'200x200':{}} ???

    @unittest_run_loop
    async def test_shoudl_raise_error_no_params_income(self):
        """ Test should check income params ans initial state """
        fb_manager = Facebook(self.app)
        with self.assertRaises(HttpErrorException):
            await fb_manager.get_user_token()

        provider = {'provider': {}}
        fb_manager = Facebook(self.app, provider=provider, code=self.code)
        with self.assertRaises(HttpErrorException):
            await fb_manager.get_user_token()

        with self.assertRaises(HttpErrorException):
            await fb_manager.validate_token()

        with self.assertRaises(HttpErrorException):
            await fb_manager.get_user_profile_data()

        with self.assertRaises(ValueError):
            await fb_manager.get_user_photos()

    @unittest_run_loop
    @aioresponses()
    async def test_should_raise_error_if_on_get_user_token(self, m):
        """ Test should raise error if user token not valid """
        fb_manager = Facebook(self.app, code='SomeWrongCode')
        with self.assertRaises(HttpErrorException):
            await fb_manager.get_user_token()

    @unittest_run_loop
    @aioresponses()
    async def test_should_raise_error_if_token_validation_but_validation_token_raise_error(self, m):
        """ Test shold check response on user token validatio fail """
        fb_manager = Facebook(self.app, user_access_token=self.user_access_token)
        with self.assertRaises(HttpErrorException):
            m.get(
                self.validate_token_url, status=200,
                payload=self.facebook_error_response
            )
            await fb_manager.validate_token()

    @unittest_run_loop
    @aioresponses()
    async def test_should_raise_error_on_get_user_data(self, m):
        """ Test should raise error on user get data """
        fb_manager = Facebook(self.app)
        with self.assertRaises(HttpErrorException):
            await fb_manager.get_user_profile_data()

    @unittest_run_loop
    @aioresponses()
    async def test_should_raise_validation_error_on_get_user_photo(self, m):
        """ Test should raise error on get user photo """
        sizes = ((200, 200), )
        fb_manager = Facebook(
            self.app, sizes=sizes,
            user_access_token=self.user_access_token
        )
        with self.assertRaises(HttpErrorException):
            m.get(self.user_photo_url, status=200, payload=self.facebook_error_response)
            await fb_manager.get_user_photos()

    @unittest_run_loop
    async def test_should_raise_error_if_user_access_token_not_defined(self):
        """ Test should raise error if user access token not defined """
        fb_manager = Facebook(self.app)
        with self.assertRaises(HttpErrorException):
            await fb_manager.validate_token()

        with self.assertRaises(HttpErrorException):
            await fb_manager.get_user_profile_data()

        with self.assertRaises(ValueError):
            await fb_manager.get_user_photos()

        # Reinit facebook manager with access code
        fb_manager = Facebook(self.app, user_access_token=self.user_access_token)

        # This case should raise error for get_photo_api without sizes parametr
        with self.assertRaises(ValueError):
            await fb_manager.get_user_photos()

    @unittest_run_loop
    @aioresponses()
    async def test_should_fetch_user_concated_instance_from_facebook_api(self, m):
        sizes = ((200, 200), )
        fb_manager = Facebook(
            self.app,
            code=self.code,
            sizes=sizes,
            provider=self.fb_provider
        )
        m.get(self.get_token_url, status=200, payload=self.token_response)
        m.get(
            self.validate_token_url, status=200,
            payload=self.validation_response
        )
        m.get(self.user_profile_url, status=200, payload=self.fb_user_profile_data)
        m.get(self.user_photo_url, status=200, payload=self.photo_response)
        await fb_manager.fetch_user_data()
        self.assertIsNotNone(fb_manager.token_response)
        self.assertIsNotNone(fb_manager.token_validation_data)
        self.assertIsNotNone(fb_manager.user_profile_data)
        self.assertIsNotNone(fb_manager.user_photos[0])
