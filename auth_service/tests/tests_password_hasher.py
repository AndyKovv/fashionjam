import unittest
from auth_service.password_manager import PasswordHash


class TestPasswordHasher(unittest.TestCase):

    def test_should_return_new_hashed_password_from_string(self):
        password = 'top_secret'
        hashed_password = PasswordHash.create_hash(password, 12)
        self.assertIsNotNone(hashed_password)

    def test_should_compare_to_passwords(self):
        password = 'top_secret'
        hashed_password = PasswordHash.create_hash(password, 12)
        is_equal = PasswordHash(hashed_password).is_equal(password)
        self.assertTrue(is_equal)
        not_equal = PasswordHash(hashed_password).is_equal('wrong_password')
        self.assertFalse(not_equal)
