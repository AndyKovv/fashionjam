# 2018 Andrii Kovalov andy.kovv@gmail.com

import sqlalchemy as sa
from auth_service.tests.fixtures import drop_social_provider_table
from core.helpers.test_case import AioHttpGetApp, unittest_run_loop
from auth_service.social_auth_providers import SocialProviderManager
from core.db.models.social_auth import social_providers as sp
from core.exceptions.http_exceptions import HttpErrorException


class SocialProviderTests(AioHttpGetApp):

    def setUp(self):
        super().setUp()

    async def tearDownAsync(self):
        async with self.app['db'].acquire() as conn:
            await conn.execute(sp.delete())

    @unittest_run_loop
    async def test_shoud_add_auth_provider_to_postgres_database(self):
        """ Test should add ptovider to PostgreSQL Database """
        new_provider = {
            'name': 'facebook',
            'provider_url': 'http://some-url.com',
            'redirect_url': 'http://some-url.com',
            'app_token': 'SomeAppToken',
            'app_secret': 'Some_app_secret',
            'api_version': 2.1
        }
        social_manager = SocialProviderManager(
            self.app,
            name=new_provider['name'],
            provider_url=new_provider['provider_url'],
            app_token=new_provider['app_token'],
            app_secret=new_provider['app_secret'],
            api_version=new_provider['api_version'],
            redirect_url=new_provider['redirect_url']
        )
        await social_manager.create_provider()

        query = (
            sa.select(
                [
                    sp.c.provider_id, sp.c.name, sp.c.provider_url,
                    sp.c.app_token, sp.c.is_active, sp.c.app_secret,
                    sp.c.api_version, sp.c.redirect_url
                ]
            )
            .where(sp.c.name == new_provider['name'])
        )
        async with self.app['db'].acquire() as conn:
            cursor = await conn.execute(query)
            created_provider = await cursor.fetchone()

        self.assertEqual(created_provider['name'], new_provider['name'])
        self.assertEqual(created_provider['provider_url'], new_provider['provider_url'])
        self.assertEqual(created_provider['app_token'], new_provider['app_token'])
        self.assertEqual(created_provider['app_secret'], new_provider['app_secret'])
        self.assertEqual(created_provider['api_version'], new_provider['api_version'])
        self.assertEqual(created_provider['redirect_url'], new_provider['redirect_url'])
        self.assertFalse(created_provider['is_active'])

    @unittest_run_loop
    async def test_should_raise_error_if_income_data_is_not_valid_on_create(self):
        """
            Test should raise error if income data is not valid or empty
        """
        new_provider = {
            'name': 'some_new_provider',
            'provider_url': 'somw_url',
            'redirect_url': 'http://some-url.com',
            'app_token': 'someToken',
            'app_secret': 'Some_app_secret',
            'api_version': 2.1
        }
        social_manager = SocialProviderManager(self.app, **new_provider)
        # At this point validation of provider url
        with self.assertRaises(HttpErrorException):
            await social_manager.create_provider()

        new_provider.update({'provider_url': 'http://new-provider.com/'})
        social_manager = SocialProviderManager(self.app, **new_provider)
        await social_manager.create_provider()

        # At this point validation of provider duplication
        social_manager = SocialProviderManager(self.app, **new_provider)
        with self.assertRaises(HttpErrorException):
            await social_manager.create_provider()

        # Validate url duplication
        new_provider['name'] = 'SomeNewProviderName'
        social_manager = SocialProviderManager(self.app, **new_provider)
        with self.assertRaises(HttpErrorException):
            await social_manager.create_provider()

        new_provider['app_token'] = 'someNewToken'
        social_manager = SocialProviderManager(self.app, **new_provider)
        await social_manager.create_provider()

    @unittest_run_loop
    async def test_should_get_urls_from_database(self):
        """ Test should fetch urls from database """
        new_provider = {
            'name': 'facebook',
            'provider_url': 'http://facebook.com/',
            'redirect_url': 'http://some-url.com',
            'app_token': 'SomeToken',
            'app_secret': 'Some_app_secret',
            'api_version': 2.1,
            'is_active': True,
         }
        social_manager = SocialProviderManager(self.app, **new_provider)
        await social_manager.create_provider()

        await social_manager.get_active_providers()
        serialized_providers = social_manager.data

        self.assertEqual(len(serialized_providers), 1)
        self.assertIsNotNone(serialized_providers[0].pop('provider_id'))
        self.assertEquals(serialized_providers, [new_provider])

    @unittest_run_loop
    async def test_should_set_provider_as_disable(self):
        """ Test should set provider as disable """
        new_provider = {
            'name': 'facebook',
            'provider_url': 'http://facebook.com/',
            'redirect_url': 'http://some-url.com',
            'app_token': 'SomeToken',
            'app_secret': 'Some_app_secret',
            'api_version': 2.1,
            'is_active': True,
        }
        manager = SocialProviderManager(self.app, **new_provider)
        await manager.create_provider()
        self.assertTrue(manager.provider['is_active'])

        # Get provider id after create provider
        provider_id = manager.provider['provider_id']

        # Disable created provider
        manager = SocialProviderManager(self.app, provider_id=provider_id)
        await manager.disable_provider()
        self.assertFalse(manager.provider['is_active'])

        # Check if created provider is disabled
        manager = SocialProviderManager(self.app, provider_id=provider_id)
        await manager.enable_provider()
        self.assertTrue(manager.provider['is_active'])

    @unittest_run_loop
    async def test_should_raise_error_if_provider_not_defined(self):
        """
            Test should raise error if provider not defined
            but user want change it status
        """
        new_provider = {
            'name': 'facebook',
            'provider_url': 'http://facebook.com/',
            'redirect_url': 'http://some-url.com',
            'app_token': 'SomeToken',
            'app_secret': 'Some_app_secret',
            'api_version': 2.1,
            'is_active': True,
        }
        manager = SocialProviderManager(self.app, **new_provider)
        await manager.create_provider()

        # Check raise error if no provider
        with self.assertRaises(HttpErrorException):
            await manager.disable_provider()

        with self.assertRaises(HttpErrorException):
            await manager.enable_provider()

    @unittest_run_loop
    async def test_should_raise_error_if_provider_not_found_in_database(self):
        """
            Test should check if provider exist in database for update it
            otherwise raise exception
        """
        manager = SocialProviderManager(self.app, provider_id=228)
        with self.assertRaises(HttpErrorException):
            await manager.enable_provider()

        with self.assertRaises(HttpErrorException):
            await manager.disable_provider()

    @unittest_run_loop
    async def test_should_return_one_provider_instance(self):
        """ Test should check returning one provider instance """
        manager = SocialProviderManager(self.app, provider_id=228)
        await manager.get_provider()
        self.assertEqual(manager.data, {})

        new_provider = {
            'name': 'facebook',
            'provider_url': 'http://facebook.com/',
            'redirect_url': 'http://some-url.com',
            'app_token': 'SomeToken',
            'is_active': True,
            'app_secret': 'Some_app_secret',
            'api_version': 2.1
        }

        # Create provider
        manager = SocialProviderManager(self.app, **new_provider)
        await manager.create_provider()

        provider_id = manager.provider['provider_id']

        # Get provider
        manager = SocialProviderManager(self.app, provider_id=provider_id)
        await manager.get_provider()

        self.assertEqual(manager.data.pop('provider_id'), provider_id)
        self.assertEqual(manager.data, new_provider)

    @unittest_run_loop
    async def test_should_raise_error_if_provider_id_not_defined_on_provider_get(self):
        """ Test should get exception if provider id not defined but get_provider was called """
        manager = SocialProviderManager(self.app)

        with self.assertRaises(HttpErrorException):
            await manager.get_provider()
