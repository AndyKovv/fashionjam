import jwt
import datetime as dt
import unittest
from freezegun import freeze_time
from trafaret_config import read_and_validate
from auth_service.jwt_manager import JWTManager
from config.utils import TRAFARET


class JWTManagerTests(unittest.TestCase):

    def setUp(self):
        self.config = read_and_validate('./config/fashion_jam.yaml', TRAFARET)

    def test_should_return_jwt_token_with_user_and_expire_time(self):
        user_id = 1
        jwt_token = JWTManager(self.config, user_id=user_id).create_token()
        self.assertIsNotNone(jwt_token)
        decoded_token = jwt.decode(
            jwt_token.token,
            self.config['jwt']['secret'],
            algorithms=[self.config['jwt']['algorithm']]
        )
        self.assertIsNotNone(decoded_token['exp'])
        self.assertIsNotNone(decoded_token['user_id'])

    def test_should_check_jwt_token_for_valid(self):
        user_id = 1
        jwt_token = JWTManager(
            self.config, user_id=user_id
        ).create_token().token

        jwt_manager = JWTManager(self.config, token=jwt_token)
        jwt_manager.validate()
        self.assertEqual(jwt_manager.user_id, user_id)

    # TODO ADD CLASS WITH CUSTOR ERROR EXCEPTIONS AND TEST IT
    def test_should_raize_error_if_token_exp_and_if_token_invalid(self):
        user_id = 1
        jwt_token = JWTManager(self.config, user_id=user_id).create_token()
        half_month = dt.datetime.utcnow() + dt.timedelta(days=15)
        with freeze_time(half_month):
            manager = JWTManager(self.config, token=jwt_token)
            self.assertFalse(manager.validate())
