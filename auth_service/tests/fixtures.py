# 2018 Andrii Kovalov andy.kovv@gmail.com

import string
import random

from core.db.user.tables import users
from core.db.models.social_auth import social_providers
from auth_service.password_manager import PasswordHash
from auth_service.social_auth_providers import SocialProviderManager


def get_random_string(length, stringset=string.ascii_letters):
    return ''.join(random.choice(stringset) for i in range(length))


async def create_user(db, email=None, password='top_secret', **kwargs):
    """
        Method should create user,
        if user_name not defined use random name
        :db - app['db'] instance
    """
    _email = email
    if not _email:
        # Generate random email
        email_string = get_random_string(10)
        _email = f"{email_string}@gmail.com"
    user_data = {
        'email': _email,
        'password': PasswordHash.create_hash(password),
        'sex': kwargs.get('sex', 'male'),
        'first_name': kwargs.get('first_name', 'Andy'),
    }
    async with db.acquire() as conn:
        cursor = await conn.execute(
            users.insert()
            .values(**user_data)
            .returning(*users.c)
        )
        user = await cursor.fetchone()
        return user


async def drop_social_provider_table(app):
    """ Procedure should drop social_provider table """
    async with app['db'].acquire() as conn:
        await conn.execute(social_providers.delete())
    return True

async def drop_user_table(app):
    """ Procedure should drop users table """
    async with app['db'].acquire() as conn:
        await conn.execute(users.delete())
    return True


class FacebookProviderFixtures:
    """
        Class implements factory with facebook methods
    """

    def __init__(self, code='SomeCode', **provider_data):
        self.__provider_data = provider_data
        self.__code = code
        self.__token_response = {
            'access_token': 'SomeFacebookUserToken',
            'token_type': 'bearer',
            'expires_in': 5179653,
        }
        self.__user_data = {
            'first_name': 'Andy',
            'gender':  'male',
            'email': 'andy.kovv@gmail.com',
            'id': '363457047398968',
            'birthday': '07/22/1991',
        }
        self.__validation_response = {
            "data": {
               "app_id": '111905079617932',
               "type": 'USER',
               "application": 'fashionjam_test',
               "expires_at": 1521058532,
               "is_valid": True,
               "issued_at": 1515874532,
               "scopes": [
                  "public_profile"
               ],
               "user_id": '363457047398968'
            }
        }
        self.__default_height = 200
        self.__default_width = 200

    @property
    def fb_user_data(self):
        return self.__user_data

    @property
    def token_data(self):
        return self.__token_response

    @property
    def validation_data(self):
        return self.__validation_response

    async def add_provider(self, app, **provider_data):
        """ Method should return created instance """
        # Custom data for create provider
        data = {
            'name': provider_data.get('name', 'facebook'),
            'provider_url': provider_data.get('url', 'http://facebook.com/'),
            'redirect_url': provider_data.get('redirecr_url', 'http://some_url.com'),
            'app_token': provider_data.get('token', 'SomeToken'),
            'app_secret': provider_data.get('secret', 'Some_app_secret'),
            'api_version': provider_data.get('version', 2.1),
            'is_active': provider_data.get('active', True),
        }
        social_manager = SocialProviderManager(app, **data)
        await social_manager.create_provider()
        self.__provider_data = social_manager.provider
        return social_manager.provider

    def create_facebook_requests_mock(self, m):
        """ Procedure should create mocks for all facebook api requests """
        # Mock request token fetch request
        m.get(
            self.create_token_fetch_url(), status=200,
            payload=self.create_token_response()
        )
        # Mock validate token request
        m.get(
            self.create_token_validation_url(), status=200,
            payload=self.create_validation_response()
        )
        # Mock user profile url
        m.get(
            self.create_user_profile_url(), status=200,
            payload=self.create_user_profile_data_response()
        )
        # Get photo for one user picture
        m.get(
            self.create_user_photo_url(), status=200,
            payload=self.create_photo_response()
        )
        return True

    def create_token_response(self, **params):
        """
            Method should return response payload
            after client code validation
        """
        access_token = self.__get_access_token(**params)
        token_type = params.get('token_type', self.__token_response['token_type'])
        expires_in = params.get('expires_in', self.__token_response['expires_in'])
        return {
            'access_token': access_token,
            'token_type': token_type,
            'expires_in': expires_in,
        }

    def create_validation_response(self, **params):
        """
            Method should return response with
            token validation information
        """
        app_id = self.__validation_response.get("app_id")
        instance_type = self.__validation_response("type")
        application = self.__validation_response.get("application")
        expires_at = self.__validation_response.get("expires_at")
        is_valid = self.__validation_response.get("is_valid")
        issued_at = self.__validation_response.get("issued_at")
        user_id = self.__validation_response.get("user_id")

        return {
            "data": {
               "app_id": app_id,
               "type": instance_type,
               "application": application,
               "expires_at": expires_at,
               "is_valid": is_valid,
               "issued_at": issued_at,
               "scopes": [
                  "public_profile"
               ],
               "user_id": user_id
            }
        }

    def update_validation_response(self, **params):
        """ Procedure should update validation response """
        self.__validation_response['data'].update(params)
        return True

    def create_error_response(self, **params):
        """ Method should create error response after api call """
        return {
            "error": {
               "message": params.get('message',  'This authorization code has expired.'),
               "type": params.get('type', 'OAuthException'),
               "code": params.get('code', 100),
               "fbtrace_id": params.get('fbtrace_id', 'Gz+HNgVJ2tO')
            }
        }

    def create_user_profile_data_response(self, **params):
        """ Method should create profile data response """
        first_name = self.__get_first_name(**params)
        gender = self.__get_gender(**params)
        id = self.__get_id(**params)
        email = self.__get_email(**params)
        birthday = self.__get_birthday(**params)
        return {
            'first_name': first_name,
            'gender': gender,
            'email': email,
            'id': id,
            'birthday': birthday,
        }

    def __get_first_name(self, **params):
        """ Method should return user first name """
        key = 'first_name'
        return params.get(key, self.__user_data(key))

    def __get_gender(self, **params):
        """ Method should return gender """
        key = 'gender'
        return params.get(key, self.__user_data(key))

    def __get_email(self, **params):
        """ Procedure should return user email """
        key = 'email'
        return params.get(key, self.__user_data(key))

    def __get_id(self, **params):
        """ Method should return facebook user id """
        key = 'id'
        return params.get(key, self.__user_data(key))

    def __get_birthday(self, **params):
        """ Method should return birthday """
        key = 'birthday'
        return params.get(key, self.__user_data(key))

    def create_photo_response(self, **params):
        """
            Method should create response
            with photo data after photo api was called
        """
        height = params.get('height', self.__default_height)
        width = params.get('width', self.__default_width)
        return {
            "data": {
                "height": height,
                "is_silhouette": params.get('is_silhouette', False),
                "url": (
                    f"https://scontent.xx.fbcdn.net/v/t1.0-1/p{height}x{width}/"
                    f"14955882_209667042777970_624717026356775021_n.jpg?oh="
                    f"ff3dc26c431e548c6555b1037baa00ab&oe=5AFBDC5A"
                ),
                "width": width
            }
        }

    def create_token_fetch_url(self, **params):
        """ Method should create url for get token after client get code """
        api_version = self.__get_api_version(**params)
        app_token = self.__get_app_token(**params)
        redirect_url = self.__get_redirect_url(**params)
        code = params.get('code', self.__code)
        return (
            f"https://graph.facebook.com/{api_version}"
            f"/oauth/access_token?client_id={app_token}"
            f"&redirect_uri={redirect_url}&client_secret={code}"
        )

    def create_token_validation_url(self, **params):
        """ Method should create url for token validation """
        access_token = self.__get_access_token(**params)
        app_token = self.__get_app_token(**params)
        app_secret = self.__get_app_secret(**params)
        return (
            f"https://graph.facebook.com/debug_token?"
            f"input_token={access_token}"
            f"&access_token={app_token}|{app_secret}"
        )

    def create_user_profile_url(self, **params):
        """ Method should create user profile url """
        access_token = self.__get_access_token(**params)
        return (
            f"https://graph.facebook.com/me?fields="
            f"id,first_name,birthday,email,gender&access_token="
            f"{access_token}"
        )

    def create_user_photo_url(self, **params):
        access_token = self.__get_access_token(**params)
        height = params.get('height', self.__default_height)
        width = params.get('width', self.__default_width)
        return (
            f"https://graph.facebook.com/me/picture?"
            f"height={height}&width={width}&access_token="
            f"{access_token}&redirect=0"
        )

    def update_user_profile_response_data(self, **params):
        """ Procedure should update profile data """
        if not params:
            return False
        self.__user_data.update(params)
        return True

    def update_token_response(self, **param):
        # Procedure should update default params if new is in
        if not param:
            return False
        self.__token_response.update(param)
        return True

    def __get_api_version(self, **params):
        return params.get('api_version', self.__provider_data['api_version'])

    def __get_app_token(self, **params):
        return params.get('app_token', self.__provider_data['app_token'])

    def __get_app_secret(self, **params):
        return params.get('app_secret', self.__provider_data['app_secret'])

    def __get_redirect_url(self, **params):
        return params.get('redirect_url', self.__provider_data['redirecr_url'])

    def __get_access_token(self, **params):
        return params.get('access_token', self.__token_response['access_token'])
