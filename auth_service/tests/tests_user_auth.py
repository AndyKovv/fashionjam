# 2018 Andrii Kovalov andy.kovv@gmail.com

import logging
import sqlalchemy as sa
from core.helpers.test_case import AioHttpGetApp, unittest_run_loop
from core.db.user.tables import users
from auth_service.password_manager import PasswordHash
from auth_service.tests.fixtures import create_user
from auth_service.constants import UserResponse as us

logger = logging.getLogger(__name__)


class RegisterApiTest(AioHttpGetApp):
    REGISTER_URL = '/api-auth/v1/register/'

    def setUp(self):
        super().setUp()

    async def tearDownAsync(self):
        async with self.app['db'].acquire() as conn:
            await conn.execute(users.delete())

    @unittest_run_loop
    async def test_should_create_user_on_registration_and_return_data(self):

        user_data = {
            'email': 'test_user@gmail.com',
            'first_name': 'NewName',
            'password': 'Adss65@as',
            'sex': 'male'
        }
        response = await self.client.post(self.REGISTER_URL, json=user_data)
        assert response.status == 200
        async with self.app['db'].acquire() as conn:
            cursor = await conn.execute(users.select())
            users_list = await cursor.fetchall()
            users_ids = [user.user_id for user in users_list]
            self.assertEqual(len(users_ids), 1)
            created_user_id = users_ids[0]
            query = (
                sa.select(
                    [
                        users.c.user_id, users.c.email, users.c.password,
                        users.c.sex, users.c.first_name
                    ]
                )
                .where(users.c.user_id == created_user_id)
            )
            cursor = await conn.execute(query)
            user = await cursor.fetchone()

            self.assertEqual(user['email'], user_data['email'])
            self.assertEqual(user['sex'], user_data['sex'])
            self.assertEqual(user['first_name'], user_data['first_name'])
            hash_passw_from_db = user['password']
            raw_password = user_data['password']
            is_equal = PasswordHash(hash_passw_from_db).is_equal(raw_password)
            self.assertTrue(is_equal)

            # check Response from api
            user_data = await self.from_json(response)
            user_id = user['user_id']
            self.assertEqual(user_data['user_id'], user_id)
            self.assertIsNotNone(user_data['token'])

    @unittest_run_loop
    async def test_should_raise_error_if_data_not_valid(self):
        user_data = {
            'email': 'tests_user_2@gmail.com',
            'first_name': 'SecondName',
            'password': 'Adss65@as',
            'sex': 'uknown'
        }
        response = await self.client.post(self.REGISTER_URL, json=user_data)
        self.assertEqual(response.status, 400)
        response_data = await self.from_json(response)
        self.assertIsNotNone(response_data['errors'])

    @unittest_run_loop
    async def test_should_raise_password_validation_if_password_not_match(self):
        user_data = {
            'email': 'test_2_mail@g.com',
            'first_name': 'MyFirstName',
            'password': 'no',
            'sex': 'male',
        }
        response = await self.client.post(self.REGISTER_URL, json=user_data)
        self.assertEqual(response.status, 400)
        response_data = await self.from_json(response)
        self.assertIsNotNone(response_data['errors'])

        # Update user password
        user_data.update({'password': 'aswedse'})
        response = await self.client.post(self.REGISTER_URL, json=user_data)
        self.assertEqual(response.status, 400)
        response_data = await self.from_json(response)
        self.assertIsNotNone(response_data['errors'])

        user_data.update({'password': 'asdDsds32@'})
        response = await self.client.post(self.REGISTER_URL, json=user_data)
        self.assertEqual(response.status, 200)


class AuthUserApiTest(AioHttpGetApp):
    LOGIN_URL = '/api-auth/v1/login/'

    async def setUpAsync(self):
        self.user = await create_user(self.app['db'])

    async def tearDownAsync(self):
        async with self.app['db'].acquire() as conn:
            await conn.execute(users.delete())

    @unittest_run_loop
    async def test_should_login_user_and_return_user_data(self):
        login_data = {
            'email': self.user['email'],
            'password': 'top_secret'
        }
        response = await self.client.post(self.LOGIN_URL, json=login_data)
        self.assertEqual(response.status, 200)
        data = await self.from_json(response)
        self.assertEqual(data['user_id'], self.user['user_id'])
        self.assertIsNotNone(data['token'])

    @unittest_run_loop
    async def test_should_return_error_if_user_invalid(self):
        login_data = {
            'email': self.user['email'],
            'password': 'new_pass'
        }
        response = await self.client.post(self.LOGIN_URL, json=login_data)
        self.assertEqual(response.status, 401)
        data = await self.from_json(response)
        self.assertEqual(data['errors'], us.ERROR_AUTH.name)
