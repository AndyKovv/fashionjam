# 2018 Andrii Kovalov andy.kovv@gmail.com

from aioresponses import aioresponses
from core.helpers.test_case import AioHttpGetApp, unittest_run_loop
from auth_service.tests.fixtures import (
    drop_social_provider_table, drop_user_table,
    FacebookProviderFixtures
)
from auth_service.auth_api import FacebookRegisterAPI
# from core.user import User
# from core.contrib.time_utils import DateTimeConverter

# core.user import User
# core.db.models.user

# for colum in users.columns._all_columns:
#     print(colum.name)

# https://github.com/ivankorobkov/python-inject


class FacebooAuthApiTests(AioHttpGetApp):
    """ Facebook auth api tests """

    FB_LOGIN_URL = 'api-auth/v1/facebook-register/'
    FB_REGISTER_URL = '/api-auth/v1/facebook-register/'

    async def setUpAsync(self):
        self.facebook_factory = FacebookProviderFixtures()
        self.facebook_provider = await self.facebook_factory.add_provider(self.app)
        self.fb_token_response = {
            'access_token': 'Face23BookAccess23324Token'
        }
        self.facebook_factory.update_token_response(**self.fb_token_response)

    async def tearDownAsync(self):
        await drop_user_table(self.app)
        await drop_social_provider_table(self.app)

    @unittest_run_loop
    @aioresponses()
    async def test_should_add_user_from_facebook_api_to_database(self, m):
        """ Test should add user from facebook api and add user data to database """

        # Create mocks for all facebook api requests
        self.facebook_factory.create_facebook_requests_mock(m)

        request_data = {
            'code': 'someFacebookCode',
        }
        response = await self.client.post(self.FB_REGISTER_URL, json=request_data)
        self.assertEqual(response.status, 200)
        data = await self.from_json(response)
        self.assertIsNotNone(data['token'])

        # At this point we check if all data added to user table
        token = data['token']

        user = User(token=token)
        user_db_instance = await user.db.get_user()

        fb_user_data = self.facebook_factory.fb_user_data
        self.assertEqual(user_db_instance.email, fb_user_data['email'])
        self.assertEqual(user_db_instance.first_name, fb_user_data['first_name'])
        self.assertEqual(user_db_instance.gender, fb_user_data['gender'])

        python_datetime = DateTimeConverter(date=fb_user_data['birth_day']).to_date_obj()
        self.assertEqual(user_db_instance.birth_day, python_datetime)

        self.assertEqual(user_db_instance.social_user_id, fb_user_data['id'])
        token_data = self.facebook_factory.token_data
        self.assertEqual(user_db_instance.social_token, token_data['access_token'])
        self.assertTrue(user_db_instance.is_valid)

        user_validation_data = self.facebook_factory.validation_data
        exp_data = DateTimeConverter(timestamp=user_validation_data['data']['expires_at']).to_date_obj()
        self.assertEqual(user_db_instance.social_token_expiration, exp_data)

        photo_response = self.facebook_factory.create_photo_response()
        photo = user_db_instance.get_photo_url(200, 200)
        self.assertEqual(photo_response['data']['url'], photo)
        inner_token = user_db_instance.token
        self.assertIsNotNone(inner_token)

        # Think DBUser or ?

    @unittest_run_loop
    async def test_should_validate_income_params_and_raise_exception(self):
        """ Test should raise exception if wrong params income """
        request_data = {
            'some_key': 'some_key'
        }
        response = await self.client.post(self.FB_REGISTER_URL, json=request_data)
        self.assertEqual(response.status, 400)
        data = await self.from_json(response)
        self.assertIsNotNone(data['errors'])
