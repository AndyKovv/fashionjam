import jwt
import datetime as dt


class JWTManager:
    """ Class implements jwt manager """

    def __init__(self, jwt_config, user_id=None, token=None):
        self._config = jwt_config['jwt']
        self._exp = dt.datetime.utcnow() + dt.timedelta(
            days=self._config['exp_delta_days']
        )
        self._user_id = user_id
        self._input_token = token
        self._jwt_token = None

    def create_token(self):
        """ Method should return instance token with payload """
        # TODO ADD ERROR EXCEPTION IF USER NOT FOUND
        payload = {
            'user_id': self._user_id,
            'exp': self._exp.timestamp(),
        }
        self._jwt_token = jwt.encode(
            payload, self._config['secret'],
            self._config['algorithm']
        )
        return self

    def validate(self):
        """ Procedure should validated token """
        if not self._input_token:
            # TODO CUSTOM HTTP EXCEPTION
            return False
        try:
            token_payload = jwt.decode(
                self._input_token,
                self._config['secret'],
                algorithms=[self._config['algorithm']]
            )
        except (jwt.DecodeError, jwt.ExpiredSignatureError):
            # TODO CUSTOM HTTP EXCEPTION
            return False

        self.__validate_payload(token_payload)
        self.user_id = token_payload['user_id']
        return True

    @property
    def token(self):
        """ Method should return jwt token otherwise None """
        return self._jwt_token

    @token.setter
    def token(self, value):
        """ Method set token value """
        self.__jwt_token = value
        return True

    @property
    def user_id(self):
        """ Method should return user id """
        return self._user_id

    @user_id.setter
    def user_id(self, value):
        self._user_id = value
        return True

    def __validate_payload(self, payload):
        """
            Method should validate if all
            params is in payload
        """
        if not payload.get('user_id'):
            raise KeyError('user_id not defined after decode jwr')
