import bcrypt


class PasswordHash:
    def __init__(self, hash_):
        assert len(hash_) == 60
        assert hash_.count('$')
        self.hash = hash_
        self.rounds = int(self.hash.split('$')[2])

    def is_equal(self, raw_password):
        """ Hashes the candidat string with stored string """
        # Check if input password is string
        if not isinstance(raw_password, str):
            # TODO Raise http error
            return False

        # Convert raw password to hash and return bool value
        pass_match = bcrypt.checkpw(
            raw_password.encode('utf-8'), self.hash.encode('utf-8')
        )
        return pass_match

    def __repr__(self):
        """ Simple object representation """
        return '<{}>'.format(type(self).__name__)

    @classmethod
    def create_hash(cls, password, rounds=12):
        """ Create a PasswordHash from the given password and return str """
        encoded_password = password.encode('utf-8')
        return bcrypt.hashpw(
                encoded_password, bcrypt.gensalt(rounds)
            ).decode('utf-8')
