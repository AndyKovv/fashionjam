from enum import Enum


class UserResponse(Enum):
    """ Class implement default response for user api """
    ERROR_AUTH = 'Password or Email invalid'
    ERROR_LOGIN_FIELDS = 'Wrong email or password'
    DB_ERROR = 'User instance not saved, please try again'
    ERROR_USER_EXISTS = 'User with this email already register'
