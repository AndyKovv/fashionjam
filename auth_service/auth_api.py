import logging
from aiohttp import web

from core.db.user.tables import users
from auth_service.auth import RegisterUser, AuthUser
from core.db_connection import Postgres

logger = logging.getLogger(__name__)


class RegisterApi(web.View):

    async def post(self):
        fields = await self.request.json()
        manager = RegisterUser(self.request, fields, users)
        await manager.persist()
        response = await manager.response()
        return web.Response(text=response)


class LoginApi(web.View):

    async def post(self):
        fields = await self.request.json()
        email = fields.get('email', None)
        password = fields.get('password', None)
        manager = AuthUser(self.request.app, email, password, users)
        await manager.login()
        response = await manager.response()
        return web.Response(text=response, status=200)


# class AppMixin():

#     async def get_db(self):
#         return await init_pg()


# class User(AppMixin):

#     async def get_user(self):
#         print(await self.get_db())


class FacebookRegisterAPI(web.View):

    async def get(self):
        print(self.request.app['db'])
        connection = Postgres.get_connection()
        print(connection)
        async with connection.acquire() as conn:
            query = 'SELECT 1'
            result = await conn.execute(query)
            print(await result.fetchall())

        # # print(self.request.app['db'])
        # user = await User().get_user()
        # print(user)
        return web.Response(status=200)
        pass
