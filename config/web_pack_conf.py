import os
import json


class WebpackConf:
    """ Class for get webpack compile file """

    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

    def __init__(self, config):
        conf = config.get('webpack', {})
        self.dir = conf.get('compile_dir_name', None)
        self.file = conf.get('info_file', None)

        if not conf or not self.dir or not self.file:
            raise KeyError('Webpack config not defined, please add webpack conf to your config')

        async def __open_file(self):
            """ Method should open file otherwise rase rerror """
            file_path = os.path.join(self.BASE_DIR, self.file)
            try:
                with open(file_path) as file:
                    return json.loads(file)
            except IOError:
                raise IOError(
                    'Error reading fiel {}'.format(file_path)
                )

        async def get_static_file(self):
            """ Method should return url and path for file """
            # Open and get file, and return json file data
            file_data = await self.__open_file()
            pass

