import trafaret as T


TRAFARET = T.Dict({
    T.Key('postgres'):
        T.Dict({
            'database': T.String(),
            'user': T.String(),
            'password': T.String(),
            'host': T.String(),
            'port': T.Int(),
            'minsize': T.Int(),
            'maxsize': T.Int(),
        }),
    T.Key('redis'):
        T.Dict({
            'database': T.Int(),
            'password': T.String(),
            'encoding': T.String(),
            'host': T.String(),
            'port': T.Int(),
            'min_pool_size': T.Int(),
            'max_pool_size': T.Int(),
        }),
    T.Key('webpack'):
        T.Dict({
            'path': T.String(),
            'file': T.String(),
        }),
    T.Key('host'): T.IP,
    T.Key('port'): T.Int(),
    T.Key('jwt'):
        T.Dict({
            'secret': T.String(),
            'algorithm': T.String(),
            'exp_delta_days': T.Int()
        })
})
