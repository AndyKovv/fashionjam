#  2018 Andrii Kovalov andy.kovv@gmail.com

import ujson
import unittest
from aiohttp.test_utils import AioHTTPTestCase, unittest_run_loop  # noqa

from fashionjam.app import init_app
from core.test_utils.tear_down import TearDownMixin


class AioHttpGetApp(AioHTTPTestCase, unittest.TestCase, TearDownMixin):

    async def get_application(self):
        """ Return current app """
        app = init_app()
        return app

    async def tearDownAsync(self):
        await self.teardown_list(self.app)

    async def from_json(self, resp):
        response = await resp.text()
        return ujson.loads(response)
