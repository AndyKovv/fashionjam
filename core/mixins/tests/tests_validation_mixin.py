# 2018 Andrii Kovalov andy.kovv@gmail.com

from marshmallow import Schema, fields
from core.helpers.test_case import AioHttpGetApp, unittest_run_loop
from core.exceptions.http_exceptions import (
    HttpErrorException, FunctionNotDefined
)
from core.mixins.validation import (
    validate_with, validate_field, validate, BaseValidation
)


class ValidationSchema(Schema):

    id = fields.Integer(required=True)
    name = fields.String(required=True)


class MockedObj(BaseValidation):
    validation_class = None
    exception = None


class MockedObjectsVall(BaseValidation):
    validation_class = ValidationSchema
    exception = HttpErrorException

    def __init__(self, **user_params):
        super().__init__()
        self._raw_data = user_params
        # self._validated_data = None

    @validate_with('__validate')
    def get_somesing(self):
        """ Method should return get validation instance """
        return self._validated_data

    @validate_with('_validate_private')
    def get_somesing_private(self):
        """ Method should return get validation instance """
        return self._validated_data

    @validate_with('__not_a_func')
    def create_something(self):
        return None

    @validate_field('name')
    def return_name(self, name):
        return name

    @validate_field('id')
    def return_id(self, id):
        return id

    def __validate(self):
        """ Procedure should validate income data """
        self._validate_income()
        return True

    def _validate_private(self):
        self._validate_income()
        return True


class MockedValidationList(BaseValidation):
    validation_class = ValidationSchema
    exception = HttpErrorException

    def __init__(self, *args):
        super().__init__()
        self._raw_data = args

    @validate
    def get_some(self):
        return self._validated_data


class ValidationMixinTestsCase(AioHttpGetApp):
    """ Tests should implement testing for validation mixin """

    @unittest_run_loop
    async def test_should_raise_error_if_no_validation_class_or_exception_class_defined(self):
        """ Tests should check raise error if no validation classes """
        with self.assertRaises(AttributeError):
            MockedObj()

        with self.assertRaises(AttributeError):
            MockedObj.validation_class = ValidationSchema
            MockedObj()

        MockedObj.validation_class = ValidationSchema
        MockedObj.exception = HttpErrorException
        MockedObj()

    @unittest_run_loop
    async def test_should_validate_income_data(self):
        """ Test should validate income data"""
        user_params = {
            'id': 1,
            'name': 'Andy'
        }
        obj = MockedObjectsVall(**user_params)
        self.assertEquals(obj.get_somesing(), user_params)
        self.assertIsNotNone(obj._validated_data)

    @unittest_run_loop
    async def test_shoudl_check_validation_on_private_method(self):
        """ Test should check validation in method in private """
        user_params = {
            'id': 1,
            'name': 'Andy'
        }
        obj = MockedObjectsVall(**user_params)
        self.assertEquals(obj.get_somesing_private(), user_params)
        self.assertIsNotNone(obj._validated_data)

    @unittest_run_loop
    async def test_should_raise_validation_error(self):
        """ Test should raise validation error with validation class """
        some_wrong_params = {
            'namek': 'someInt',
            'derec': 'some Spoon'
        }
        obj = MockedObjectsVall(**some_wrong_params)
        with self.assertRaises(HttpErrorException):
            obj.get_somesing()

    @unittest_run_loop
    async def test_shoudl_raise_error_if_wrong_validation_function_income(self):
        """ Test should raise error if validation funtion not defined """
        obj = MockedObjectsVall()
        with self.assertRaises(AttributeError):
            obj.create_something()

    @unittest_run_loop
    async def test_should_validate_income_params_from_list(self):
        """ Test should validate income prams if prams in list """
        params = [
            {
                'id': 1,
                'name': 'Andy'
            },
            {
                'id': 2,
                'name': 'Kovv'
            }
        ]
        obj = MockedValidationList(*params)
        self.assertEquals(obj.get_some(), params)

    @unittest_run_loop
    async def test_should_raise_error_if_income_params_not_valid(self):
        """ Tets should raise exception if validate params is invalid"""
        params = [
            {
                'some_id': 1,
                'some_name': 'Name'
            },
            {
                'some_id': 2,
                'some_name': 'Name'
            }
        ]
        obj = MockedValidationList(*params)
        with self.assertRaises(HttpErrorException):
            obj.get_some()

    @unittest_run_loop
    async def test_should_validate_only_income_field(self):
        """ Test should validate method only with income fields """
        user_params = {
            'id': 1,
            'name': 'Andy'
        }
        obj = MockedObjectsVall(**user_params)
        result = obj.return_name('Andy')
        self.assertEqual(result, 'Andy')

    @unittest_run_loop
    async def test_should_raise_error_if_validation_params_invalid_or_empty(self):
        """ Test should check if validation params is invalid or empty """
        user_params = {
            'id': 1,
            'name': 'Andy'
        }
        obj = MockedObjectsVall(**user_params)
        with self.assertRaises(MockedObjectsVall.exception):
            obj.return_id('Andy')
