
class ErrorFormatterMixin:
    """ Class implement default error format structure """

    def __init__(self):
        self.dict_error = {
            'error': {}
        }
        self.string_error = {
            'error': None
        }
        self.__error_exist = False

    # TODO ADD UNITTESTS
    def add_key_error(self, key, message):
        """ Procedure should add error key with message """
        self.dict_error['error'].update({key: message})
        self.error_exist = True
        return True

    # TODO ADD UNITTESTS
    def add_string_error(self, message):
        """ Method should return standart string error """
        self.string_error.update({'error': message})
        self.error_exist = True
        return True

    # TODO ADD UNITTESTS
    def get_error(self):
        """ Method should check wich error updated and return it """
        if not self.dict_error['error']:
            return self.string_error
        return self.dict_error

    @property
    def error_exist(self):
        """ Procedure should check if error are not empty and return boolean value """
        return self.__error_exist

    @error_exist.setter
    def error_exist(self, value):
        """ Procedure should set error_exist to True """
        self.__error_exist = value
        return True
