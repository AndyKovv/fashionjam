#2017 Andrii Kovalov andy.kovv@gmail.com

import aiohttp_jinja2
from aiohttp import web


def get_main_page(request):
    """ Function should return main page if 404 error was raised """
    response = aiohttp_jinja2.render_template('base.html', request, context={})
    return response


# TODO ADD UNITTESTS
@web.middleware
async def middleware_404(request, handler):
    """ Middleware render index page """
    try:
        response = await handler(request)
        if response.status == 404:
            return get_main_page(request)
        return response
    except web.HTTPException as ex:
        if ex.status == 404:
            return get_main_page(request)
