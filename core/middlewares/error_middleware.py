# 2017 Andriy Kovalov andy.kovv@gmail.com

import ujson
from aiohttp import web
from core.exceptions.http_exceptions import HttpErrorException


def json_error_response(payload, status):
    """ Method should returb error response """
    return web.Response(text=ujson.dumps(payload), status=status)


@web.middleware
async def error_middleware(request, handler):
    """ Middleware return custom error response if rase """
    try:
        return await handler(request)
    except HttpErrorException as ex:
        return web.Response(text=ujson.dumps(ex.payload), status=ex.status)
