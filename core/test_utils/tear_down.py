# 2018 Andrii Kovalov andy.kovv@gmail.com

from core.db.user.tables import users
from core.db_connection import Postgres


class TearDownMixin:
    """ Mixin for tear down  """
    async def teardown_list(self, app):
        """ Procedure use for executing teardown procedures """
        connection = Postgres(app.loop, app['config']).get_connection()
        await self.drop_user_table(connection)

    async def drop_user_table(self, connection):
        """ Procedure should drop users table """
        async with connection.acquire() as conn:
            await conn.execute(users.delete())
        return True