# 2018 Andrii Kovalov andy.kovv@gmail.com

import aiopg.sa
import logging
from core.db.sa_metadata import metadata
from core.exceptions.http_exceptions import HttpErrorException

logger = logging.getLogger(__name__)

class Postgres:
    """
        Class implements singletone for postgres connection
        for user connection from object need call get_connection instance

    """
    class __Postgres:

        def __init__(self, loop=None, config=None):
            self.__loop = loop
            self.__config = config['postgres']
            self.__connection = None

        @property
        def connection_instance(self):
            return self.__connection

        async def connect(self, app=None):
            global connection
            engine = await aiopg.sa.create_engine(
                database=self.__config['database'],
                user=self.__config['user'],
                password=self.__config['password'],
                host=self.__config['host'],
                port=self.__config['port'],
                minsize=self.__config['minsize'],
                maxsize=self.__config['maxsize'],
                loop=self.__loop,
            )
            self.__connection = engine
            connection = engine
            if app:
                app['db'] = engine

        def __str__(self):
            return repr(self)

    __connection = None

    def __init__(self, loop=None, config=None):
        if not Postgres.__connection:
            Postgres.__connection = Postgres.__Postgres(loop, config)

    def __getattr__(self, attr):
        return getattr(self.__connection, attr)

    @classmethod
    def get_connection(cls):
        return cls.__connection.connection_instance


class DBConnectionMixin:
    """ Connection mixin for database """

    lazy_connection = Postgres.get_connection

    @property
    def connection(self):
        return self.lazy_connection()

    async def is_inserted(self, res, query):
        """ Procedure will raise exeption if database not added value """
        # res = await result.fetchone()
        if not res:
            logger.error(f'Error insert query {query}')
            raise HttpErrorException('Error occurred, please try again', 400)
        return True


async def init_pg(app):
    conf = app['config']['postgres']
    engine = await aiopg.sa.create_engine(
        database=conf['database'],
        user=conf['user'],
        password=conf['password'],
        host=conf['host'],
        port=conf['port'],
        minsize=conf['minsize'],
        maxsize=conf['maxsize'],
        loop=app.loop,
    )
    app['db'] = engine
    app['db_declarative_metadata'] = metadata

async def close_pg(app):
    app['db'].close()
    await app['db'].wait_closed()
