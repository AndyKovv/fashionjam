from enum import Enum


class ResponseConstants(Enum):
    """ Class implements constants for response in core pacage """
    ERROR_KEY = 'error'
