#  2018 Andrii Kovalov andy.kovv@gmail.com

from marshmallow import Schema, fields


class BaseUserSchema(Schema):
    """ Base User Schema from register """

    email = fields.Email(required=True)
    password = fields.String(required=True)
    first_name = fields.String(required=True)
    sex = fields.String(required=True)
