# 2018 Andrii Kovalov andy.kovv@gmail.com

import sqlalchemy as sa
from core.exceptions.http_exceptions import HttpErrorException
from core.mixins.validation import (
    BaseValidation, validate_field,
    validate, validation_required,
    validate_with
)

from core.user.schemas import BaseUserSchema
from core.db_connection import DBConnectionMixin
from core.db.user.tables import users
from core.db_connection import Postgres

from auth_service.password_manager import PasswordHash


# Think need create BaseUserManagerClass

class BaseUser(DBConnectionMixin, BaseValidation):
    """
        Base user object used
        for save base user to database
    """

    validation_class = BaseUserSchema
    exception = HttpErrorException

    returning_user_fields = [
        users.c.user_id,
        users.c.email,
        users.c.password,
        users.c.first_name,
        users.c.sex
    ]

    def __init__(self, **params):
        self._user_id = params.get('user_id', None)
        self._raw_data = params
        self._created_user = dict()

    @property
    def created_user(self):
        """ Return created user instance """
        return self._created_user

    @validate
    async def create(self):
        """ Procedure should save user to database """
        self.__update_hashed_password()
        query = users.insert().returning(*users.c).values(self.validated_data)
        async with self.connection.acquire() as conn:
            result = await conn.execute(query)
            await self.is_inserted(result, query)
            self._created_user = await result.fetchone()
        return True

    @validate_with('_validate_user_id')
    async def get_by_id(self):
        """
            Method should return user instance
            from database fetched by id
        """

        query = sa.select(
            self.returning_user_fields
            ).where(users.c.user_id == self._user_id)

        async with self.connection.acquire() as conn:
            # TODO think how implement chek insert to database
            cursor = await conn.execute(query)
            return await cursor.fetchone()

    @validate_field('email')
    async def fetch_by_email(self, email):
        """ Method should return user proxy instance """

        query = (
            sa.select(
                self.returning_user_fields
            ).where(users.c.email == email)
        )
        async with self.connection.acquire() as conn:
            cursor = await conn.execute(query)
            return await cursor.fetchone()

    @validation_required
    def __update_hashed_password(self):
        """
            Procedure should update raw password with password hash
        """
        hashed_password = PasswordHash.create_hash(self._validated_data['password'])
        self._validated_data.update({'password': hashed_password})
        return True

    def _validate_user_id(self):
        """ Procedure should validate user_id param """
        if not self._user_id:
            raise ValueError(f'attribute user_id must be defined, or not None')
        if not isinstance(self._user_id, int):
            raise ValueError(
                f'attribute user_id must be integer not {type(self._user_id)}'
            )
        return True