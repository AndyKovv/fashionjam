# 2018 Andrii Kovalov andy.kovv@gmail.com

from core.helpers.test_case import AioHttpGetApp, unittest_run_loop
from core.user import BaseUser
from core.user.tests.user_fixtures import BaseUserFixture


class BaseUserTests(AioHttpGetApp):
    """ Tests should check DB user interface """

    def setUp(self):
        super().setUp()
        self.default_user_fields = {
            'email': 'some@email.com',
            'password': 'some_password',
            'first_name': 'Andy',
            'sex': 'male'
        }

    async def tearDownAsync(self):
        await super().tearDownAsync()

    @unittest_run_loop
    async def test_should_create_default_user_in_database(self):
        """ Test should create user in database and update instance """
        user = BaseUser(**self.default_user_fields)
        await user.create()

        saved_user = await BaseUser().fetch_by_email(self.default_user_fields['email'])
        self.assertIsNotNone(saved_user['user_id'])
        self.assertEqual(
            saved_user['email'], self.default_user_fields['email']
        )
        self.assertNotEqual(
            saved_user['password'], self.default_user_fields['password']
        )
        self.assertEqual(
            saved_user['first_name'], self.default_user_fields['first_name']
        )
        self.assertEqual(
            saved_user['sex'], self.default_user_fields['sex']
        )
        # TODO implment user register time

    @unittest_run_loop
    async def test_should_get_user_by_user_id(self):
        """ Test should get user from database by id """
        user = await BaseUserFixture.create_base_user()
        created_user_email = user['email']
        user_id = user['user_id']

        instance = BaseUser(user_id=user_id)
        saved_user = await instance.get_by_id()
        self.assertIsNotNone(saved_user)
        save_user_email = saved_user['email']
        self.assertEqual(save_user_email, created_user_email)
