# 2018 Andrii Kovalov andy.kovv@gmail.com

from core.db.user.tables import users
from core.user.schemas import BaseUserSchema
from core.user.base_user import BaseUser


class BaseDataMixin:
    """ Class implement base data mixin """

    @property
    def base_user_default_data(self):
        """ Getter should return default data for base user """
        default_data = {
            'email': 'some@email.com',
            'password': 'some_password',
            'first_name': 'Andy',
            'sex': 'male'
        }
        return default_data


class BaseUserFixture(BaseDataMixin):
    """ Class implement user fixture for testing """
    base_schema = BaseUserSchema

    @classmethod
    async def create_base_user(cls):
        """
            Method should create base user in database
            and return user instance from database
        """
        base_data = cls.base_schema().load(cls().base_user_default_data)
        instance = BaseUser(**base_data.data)
        await instance.create()
        return instance.created_user

class SocialAuthFixture(BaseDataMixin):
    """ Class implement create user from socia auth providers """
    @classmethod
    async def create_user_from_facebook(cls):
        
