# 2018 Andrii Kovalov andy.kovv@gmail.com

from core.exceptions.http_exceptions import HttpErrorException
from core.mixins.validation import BaseValidation, validate_with, validate


class BaseDBUser(BaseValidation):
    """
        Interface using postgres database
        For manage user instance

    """

    exception = HttpErrorException

    def __init__(self, **params):
        self._raw_params = params

