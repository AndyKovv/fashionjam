# 2018 Andrii Kovalov andy.kovv@gmail.com

import ujson
from marshmallow import Schema, fields


class SocialProviderSchema(Schema):
    provider_id = fields.Integer()
    name = fields.String(required=True)
    provider_url = fields.Url(required=True)
    redirect_url = fields.Url(required=True)
    app_token = fields.String(required=True)
    app_secret = fields.String(required=True)
    api_version = fields.Float(required=True)
    is_active = fields.Boolean(False)

    class Meta:
        render_module = ujson
