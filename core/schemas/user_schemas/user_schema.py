import ujson
import re
from marshmallow import Schema, fields, validates, ValidationError
from core.schemas.constants import SchemaErrorConstants as se


class UserSchema(Schema):
    """
        User schema serializer
    """

    MALE = 'male'
    FEMALE = 'female'

    SEX_LIST = (MALE, FEMALE)
    SEX_CHOICES = (
        (MALE,  'Male'),
        (FEMALE, 'Female')
    )

    user_id = fields.Integer()
    email = fields.Email(required=True)
    first_name = fields.String(required=True)
    password = fields.String(required=True)
    date_of_birth = fields.Email(required=True)
    avatar = fields.Url()
    sex = fields.String(required=True)
    is_active = fields.Boolean(True)
    is_staff = fields.Boolean(False)
    is_superuser = fields.Boolean(False)

    class Meta:
        render_module = ujson

    @validates('sex')
    def __validate_sex(self, sex):
        """ Method should validate sex chooce """
        if sex not in self.SEX_LIST:
            raise ValidationError(se.ERROR_SEX_VALUE.name)
        return sex

    @validates('password')
    def __validate_password(self, password):
        """ Method should validate password """
        match = re.match(r'^(?=.*[a-z])(?=.*\d)(?=.*[A-Z])(?:.{6,24})$', password)
        if not match:
            raise ValidationError(se.ERROR_PASSWORD_STREIGHT.name)

        return password


class UserCacheInstance(UserSchema):
    """
        User cache instance shema
        main data structure is used from UserSchema
        but additional attributes must be added here
    """
    last_seen = fields.Integer(required=True)
    password_hash = fields.String(required=True)

    class Meta:
        render_module = ujson

    @staticmethod
    def cache_user_partial():
        """
            Method should return partial fields for user cached instance
        """
        return (
            'date_of_birth', 'avatar', 'sex',
            'is_active', 'is_staff', 'is_superuser',
            'password'
        )
