# 2017 Andriy Kovalov andy.kovv@gmail.com

from enum import Enum


class SchemaErrorConstants(Enum):
    """ Class implements Schema error constants """
    ERROR_SEX_VALUE = 'Sex not defined, please choose one'
    ERROR_PASSWORD_STREIGHT = 'Password must be min 6 characters max 24 and cotain at least 1 number'
