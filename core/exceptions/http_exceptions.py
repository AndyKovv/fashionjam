# 2017 Andrii Kovalov andy.kovv@gmail.com


class HttpErrorException(Exception):
    """ Class implements custom http exception """

    def __init__(self, message, status):
        self._message = message
        self._status = status
        self._validate()

    @property
    def payload(self):
        return {'errors': self._message}

    @property
    def status(self):
        return self._status

    def _validate(self):
        """ Procedure should validate status code """
        if not isinstance(self._status, int):
            raise ValueError(
                f"status must be integer not {type(self._status)}"
            )
        return True


class FunctionNotDefined(Exception):
    """
        Custom exeption class
        will raise exception when function not defined
    """

    def __init__(self, message, func):
        self._message = message
        self._func = func
