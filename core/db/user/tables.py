# 2017 Andrii Kovalov andy.kovv@gmail.com
import sqlalchemy as sa
from core.db.sa_metadata import metadata

users = sa.Table(
     'users',
     metadata,
     sa.Column('user_id', sa.Integer, primary_key=True),
     sa.Column('email', sa.String(255), nullable=False),
     sa.Column('password', sa.String(255), nullable=False),
     sa.Column('first_name', sa.String(255), nullable=False),
     sa.Column('date_of_birth', sa.DateTime, nullable=True),
     sa.Column('avatars', sa.JSON, nullable=True),
     sa.Column('sex', sa.String, nullable=False),
     sa.Column('is_active', sa.Boolean, default=True),
     sa.Column('is_staff', sa.Boolean, default=False),
     sa.Column('is_superuser', sa.Boolean, default=False),
     sa.Column('token', sa.String(255), nullable=True),

     sa.Column('social_user_id', sa.Integer, nullable=True),
     sa.Column('social_token', sa.String(255), nullable=True),
     sa.Column('social_token_expiration', sa.String(255), nullable=True),

     sa.Column('social_provider_id', sa.ForeignKey('social_providers.provider_id'), nullable=True)
)
