# 2018 Andy Kovv andy.kovv@gmail.com
# Model used with user social auth
import sqlalchemy as sa
from core.db.sa_metadata import metadata

social_providers = sa.Table(
    'social_providers',
    metadata,
    sa.Column('provider_id', sa.Integer, primary_key=True),
    sa.Column('name', sa.String(255), nullable=False),
    sa.Column('provider_url', sa.String(255), nullable=False),
    sa.Column('redirect_url', sa.String(255), nullable=False),
    sa.Column('app_token', sa.String(255), nullable=False),
    sa.Column('app_secret', sa.String(255), nullable=False),
    sa.Column('api_version', sa.Float, nullable=False),
    sa.Column('is_active', sa.Boolean, default=False)
)
