"""add redirect url

Revision ID: 56c4fbafd8f5
Revises: 340d8b060658
Create Date: 2018-01-23 11:40:06.368473

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '56c4fbafd8f5'
down_revision = '340d8b060658'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('social_providers', sa.Column('redirect_url', sa.String(length=255), nullable=False))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('social_providers', 'redirect_url')
    # ### end Alembic commands ###
