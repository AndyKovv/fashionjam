"""

Revision ID: 340d8b060658
Revises: aaffc0c5a778
Create Date: 2018-01-22 22:43:44.895223

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '340d8b060658'
down_revision = 'aaffc0c5a778'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('social_providers', sa.Column('api_version', sa.Float(), nullable=False))
    op.add_column('social_providers', sa.Column('app_secret', sa.String(length=255), nullable=False))
    op.add_column('users', sa.Column('social_user_id', sa.Integer(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('users', 'social_user_id')
    op.drop_column('social_providers', 'app_secret')
    op.drop_column('social_providers', 'api_version')
    # ### end Alembic commands ###
